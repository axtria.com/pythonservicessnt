import os
import logging
import psycopg2 as ps
import simple_salesforce

from flask import Flask, request, send_file
from logging.handlers import TimedRotatingFileHandler

from src.gateway.job import Job
from src.service.util import fetch_parameter


app = Flask(__name__)
PREFIX = '/data_services_61'
METHODS = ['POST','GET']


@app.route(PREFIX + "/brDeltaPullDataSources",methods=METHODS)
def delta_sfdc_to_postgres():
    def remove_from_queue(scenario_id):
        if scenario_id:
            with open(os.path.join('src', "scenarios_in_progress.txt"), 'r+') as f:
                a = f.read()
                a = a.replace(scenario_id, '')
                f.truncate(0)
                f.seek(0)
                f.write(a)
            logging.debug("Scenario {} removed from 'In Progress'".format(scenario_id))

    data_object_ids = fetch_parameter(request, 'dataObjectIds', default='1', type=str)
    scenario_id = fetch_parameter(request, 'scenario_id', default='', type=str)
    user_name = fetch_parameter(request, 'user_name', default='1', type=str)
    password = fetch_parameter(request, 'password', default='1', type=str)
    security_token = fetch_parameter(request, 'security_token', default='', type=str)
    namespace = fetch_parameter(request, 'namespace', default='', type=str)
    sandbox = fetch_parameter(request, 'sandbox', default='false', type=str)
    logger_id = fetch_parameter(request, 'logger_id',default='',type=str)
    logger_object_name = fetch_parameter(request, 'logger_object_name', default = '', type=str)
    
    sandbox = True if sandbox.lower() == 'true' else False
    
    for i in range(2):
        logging.debug("Trying again") if i > 0 else ''
        try:
            job = Job()
            job.build('DeltaSfdcToPostgres', {
                'data_object_ids': data_object_ids,
                'scenario_id': scenario_id,
                'user_name': user_name,
                'password': password,
                'security_token': security_token,
                'namespace': namespace,
                'sandbox': sandbox,
                'logger_id': logger_id,
                'logger_object_name': logger_object_name,
                'called_independently': True

            })
            output = job.execute_all()
        except ps.OperationalError as p:
            logging.critical(p)
            remove_from_queue(scenario_id)
            return "Too many connections too Postgres. Can't create another. Halting service"

        except simple_salesforce.exceptions.SalesforceAuthenticationFailed as s:
            logging.critical(s)
            remove_from_queue(scenario_id)
            return str(s)

        except Exception as e:
            logging.exception("ERROR")
            remove_from_queue(scenario_id)
            return "unhandled exception. Check logs for more details."
        
        logging.debug(output)
        if not output.strip():
            break
    remove_from_queue(scenario_id)
    return " "


@app.route(PREFIX + "/brDeltaPushResultToDestination",methods=METHODS)
def delta_postgres_to_sfdc():
    dataset_id = fetch_parameter(request, 'upstream_dataset_id', default='1', type=str)
    scenario_id = fetch_parameter(request, 'scenario_id', default=None, type=str)
    do_promote = fetch_parameter(request, 'do_promote', default='false', type=str)
    user_name = fetch_parameter(request, 'user_name', default='1', type=str)
    password = fetch_parameter(request, 'password', default='1', type=str)
    security_token = fetch_parameter(request, 'security_token', default='', type=str)
    table_name = fetch_parameter(request, 'upstream_table_name', default='', type=str)
    namespace = fetch_parameter(request, 'namespace', default='', type=str)
    sandbox = fetch_parameter(request, 'sandbox', default='False', type=str)
    is_delta = fetch_parameter(request, 'isDelta', default='false', type=str)
    compare_table_name = fetch_parameter(request, 'compare_table_name', default='', type=str)
    compare_table_filter = fetch_parameter(request, 'compare_table_filter', default='', type=str)
    compare_dataset_id = fetch_parameter(request, 'compare_dataset_id', default='', type=str)
    customer_univ_table_name = fetch_parameter(request, 'customer_univ_table_name', default='', type=str)
    customer_univ_dataset_id = fetch_parameter(request, 'customer_univ_dataset_id', default='', type=str)

    job = Job()
    sandbox = True if sandbox.lower() == 'true' else False
    do_promote_bool = True if do_promote.lower() == 'true' else False
    job.build('DeltaPostgresToSfdc', {
        'dataset_id': dataset_id,
        'scenario_id': scenario_id,
        'table_name': table_name,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'do_promote': do_promote_bool,
        'namespace': namespace,
        'sandbox': sandbox,
        'is_delta': is_delta.lower(),
        'compare_table_name': compare_table_name,
        'compare_table_filter': compare_table_filter,
        'compare_dataset_id': compare_dataset_id,
        'customer_univ_table_name': customer_univ_table_name,
        'customer_univ_dataset_id': customer_univ_dataset_id
    })
    job.execute_all()
    return " "


@app.route(PREFIX + "/brDeltaExecuteScenario",methods=METHODS)
def delta_execute_talend_job():
    def remove_from_queue(scenario_id):
        with open(os.path.join('src', "scenarios_in_progress.txt"), 'r+') as f:
            a = f.read()
            a = a.replace(scenario_id, '')
            f.truncate(0)
            f.seek(0)
            f.write(a)
        logging.debug("Scenario {} removed from 'In Progress'".format(scenario_id))

    data_object_ids = fetch_parameter(request, 'dataObjects_ids', default='1', type=str)
    do_promote = fetch_parameter(request, 'do_promote', default='false', type=str)
    upstream_dataset_id = fetch_parameter(request, 'upstream_dataset_id', default='', type=str)
    upstream_table_name = fetch_parameter(request, 'upstream_table_name', default='', type=str)
    user_name = fetch_parameter(request, 'user_name', default='', type=str)
    password = fetch_parameter(request, 'password', default='', type=str)
    security_token = fetch_parameter(request, 'security_token', default='', type=str)
    scenario_id = fetch_parameter(request, 'scenario_id', default=None, type=str)
    namespace = fetch_parameter(request, 'namespace', default='', type=str)
    is_delta = fetch_parameter(request, 'isDelta', default='false', type=str)
    sandbox = fetch_parameter(request, 'sandbox', default='False', type=str)
    compare_table_name = fetch_parameter(request, 'compare_table_name', default='', type=str)
    compare_table_filter = fetch_parameter(request, 'compare_table_filter', default='', type=str)
    compare_dataset_id = fetch_parameter(request, 'compare_dataset_id', default='', type=str)
    customer_univ_table_name = fetch_parameter(request, 'customer_univ_table_name', default='', type=str)
    customer_univ_dataset_id = fetch_parameter(request, 'customer_univ_dataset_id', default='', type=str)
    logger_id = fetch_parameter(request, 'logger_id',default='',type=str)
    logger_object_name = fetch_parameter(request, 'logger_object_name', default = '', type=str)

    sandbox = True if sandbox.lower() == 'true' else False

    for i in range(2):
        logging.debug("Trying it one more time") if i > 0 else ''
        try:
            job = Job()
            if is_delta.lower() == 'false':
                job.build('DeltaSfdcToPostgres', {
                    'data_object_ids': data_object_ids,
                    'scenario_id': scenario_id,
                    'user_name': user_name,
                    'password': password,
                    'security_token': security_token,
                    'namespace': namespace,
                    'sandbox': sandbox,
                    'logger_id': logger_id,
                    'logger_object_name': logger_object_name,
                    'called_independently': False
                })
            job.build('SfdcToPostgresMetadataCopy', {
                'scenario_id': scenario_id,
                'user_name': user_name,
                'password': password,
                'security_token': security_token,
                'namespace': namespace,
                'sandbox': sandbox
            })
            do_promote_bool = True if do_promote.lower() == 'true' else False
            job.build('ExecuteScenario', {
                'do_promote': do_promote_bool,
                'dataset_id': upstream_dataset_id,
                'table_name': upstream_table_name,
                'scenario_id': scenario_id,
                'user_name': user_name,
                'password': password,
                'security_token': security_token,
                'namespace': namespace,
                'is_delta': is_delta.lower(),
                'sandbox': sandbox,
                'compare_table_name': compare_table_name,
                'compare_table_filter': compare_table_filter,
                'compare_dataset_id': compare_dataset_id,
                'customer_univ_table_name': customer_univ_table_name,
                'customer_univ_dataset_id': customer_univ_dataset_id
            })
            job.build('PromoteFromPostgres', {
                'do_promote': do_promote_bool,
                'dataset_id': upstream_dataset_id,
                'scenario_id': scenario_id,
                'user_name': user_name,
                'password': password,
                'security_token': security_token,
                'namespace': namespace,
                'sandbox': sandbox,
                'ip_addr': request.host,
                'prefix' : PREFIX
            })
            output = job.execute_all()
        except ps.OperationalError as p:
            logging.critical(p)
            remove_from_queue(scenario_id)
            return "Too many connections too Postgres. Can't create another. Halting service"

        except simple_salesforce.exceptions.SalesforceAuthenticationFailed as s:
            logging.critical(s)
            remove_from_queue(scenario_id)
            return str(s)

        except Exception as e:
            logging.exception("ERROR")
            remove_from_queue(scenario_id)
            return "unhandled exception. Check logs for more details."

        logging.debug(output)
        if not output.strip():
            break
    remove_from_queue(scenario_id)
    return output

@app.route(PREFIX + "/brMasterSyncRaw",methods=METHODS)
def master_sync_raw():
    data_object_ids = fetch_parameter(request, 'dataObjectIds', default='1', type=str)
    scenario_id = fetch_parameter(request, 'scenario_id', default=None, type=str)
    user_name = fetch_parameter(request, 'user_name', default='1', type=str)
    password = fetch_parameter(request, 'password', default='1', type=str)
    security_token = fetch_parameter(request, 'security_token', default='', type=str)
    namespace = fetch_parameter(request, 'namespace', default='', type=str)
    sandbox = fetch_parameter(request, 'sandbox', default='False', type=str)
    job = Job()
    sandbox = True if sandbox.lower() == 'true' else False
    job.build('Master_Sync_Raw', {
        'data_object_ids': data_object_ids,
        'scenario_id': scenario_id,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'namespace': namespace,
        'sandbox': sandbox
    })
    job.execute_all()
    return " "

@app.route(PREFIX + '/download/<fname>/<attach>', methods=METHODS)
def download(fname,attach):
    logging.debug("sending file as attachment")
    # return os.path.abspath(os.path.join('tmp',fname))
    return send_file(os.path.abspath(os.path.join('promote_tmp',fname)), mimetype='application/text',
                     as_attachment=True,attachment_filename=attach+fname)

@app.before_request
def log_request_info():
    logging.debug('Request ::: %r' % request.url)
    scenario_id = fetch_parameter(request, 'scenario_id', default="", type=str)
    if scenario_id:
        with open(os.path.join('src', 'scenarios_in_progress.txt'), 'r+') as f:
            a = f.read()
            if a.find(scenario_id) != -1:
                logging.critical("Duplicate Hit! Scenario {} is already in progress.".format(scenario_id))
                return "Duplicate Hit! Scenario {} is already in progress.".format(scenario_id)
            else:
                f.truncate(0)
                f.seek(0)
                f.write(a + scenario_id)
        logging.debug("Scenario {} has been logged as 'In Progress'".format(scenario_id))

logging.basicConfig(level=logging.DEBUG,
                        handlers=[TimedRotatingFileHandler("logs//system.log", when="midnight"),
                                  logging.StreamHandler()],
                        format='%(asctime)s - %(module)s.%(funcName)s() - %(levelname)s - %(message)s')
logging.getLogger('requests').setLevel(logging.CRITICAL)
logging.getLogger('urllib3').setLevel(logging.CRITICAL)

if __name__ == '__main__':
    # app.debug = True
    logging.basicConfig(level=logging.DEBUG,
                        handlers=[TimedRotatingFileHandler("logs//system.log", when="midnight"),
                                  logging.StreamHandler()],
                        format='%(asctime)s - %(module)s.%(funcName)s() - %(levelname)s - %(message)s')
    logging.getLogger('requests').setLevel(logging.CRITICAL)
    logging.getLogger('urllib3').setLevel(logging.CRITICAL)

    app.run()
