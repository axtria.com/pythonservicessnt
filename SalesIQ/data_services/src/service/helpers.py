import logging
from functools import wraps
from threading import Thread
from time import time


def thread(func):
    """
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        thread_func = Thread(target=func, args=args, kwargs=kwargs, daemon=True)
        thread_func.start()
        return thread_func

    return wrapper


def timer(func):
    """
    function decorator to time the execution of a function
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        start = time()
        result = func(*args, **kwargs)
        end = time()
        logging.info("%s - %s : %4.2f minute(s)" % (args[0].__class__.__name__, func.__name__, (end - start) / 60))
        return result

    return wrapper


def debugger(func):
    """
    function decorator to log execution of a function and the result returned
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        # Todo: Add input logging INFO
        result = func(*args, **kwargs)
        logging.debug(":::: %s.%s() --> %r" % (args[0].__class__.__name__, func.__name__, result))
        return result

    return wrapper