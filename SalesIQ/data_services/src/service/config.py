"""
    data_services_config
    ~~~~~~~~~~~

    Hold the configuration file - config.ini into a global object of class type ConfigParser

"""

from configparser import ConfigParser

data_services_config = ConfigParser()
data_services_config.read('./config.ini')
