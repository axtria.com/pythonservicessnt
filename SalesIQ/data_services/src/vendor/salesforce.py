import os
import re
import json
import logging
import requests
import pandas as pd

from time import sleep
from uuid import uuid4
from datetime import datetime
from multiprocessing import Process, Lock, Value

from src.service.helpers import timer
from salesforce_bulk import SalesforceBulk
from simple_salesforce import Salesforce, exceptions
from exclusions import unmanaged_objects, unmanaged_fields

logging.getLogger('urllib3').setLevel(logging.CRITICAL)

session  = requests.Session()

SF_VERSION_NEW   = "45.0" 
BULK_V2_URI      = 'jobs/ingest/'
BATCH_TIMEOUT    = 120
BATCH_ATTEMPTS   = 10
BATCH_SLEEP_TIME = 2
TIMEOUT_ATTEMPTS = 3


class SalesforceConnector:
    pool = {}

    def __init__(self, username, password, security_token, namespace, sandbox):
        self.namespace = namespace
        # try:
        self.sf = Salesforce(username=username, password=password, security_token=security_token, sandbox=sandbox)
        self.bulk_sf = SalesforceBulk(username=username, password=password, security_token=security_token,
                                      sandbox=sandbox)
        self.sf_v2 = SalesForce_V2(self.sf)
        # except Exception as e:
        #     logging.error(e)

    @classmethod
    def initialise_from_pool(cls, username: str, password: str, security_token: str, namespace: str, sandbox: bool):
        if username + password not in SalesforceConnector.pool:
            logging.debug("Registering new connection in pool for %s " % username)
            SalesforceConnector.pool[username + password] = cls(username, password, security_token, namespace, sandbox)
        else:
            salesforce = SalesforceConnector.pool.get(username + password)
            try:
                logging.debug("Checking if SF connection has expired")
                salesforce.query("SELECT count() FROM Account LIMIT 1")
                logging.debug("SF connection OK!")
            except Exception as e:
                logging.error(e)
                logging.error("Salesforce connection has expired. Re-establishing and recycling the pool")
                SalesforceConnector.pool.pop(username + password)
                SalesforceConnector.pool[username + password] = cls(username, password, security_token, namespace, sandbox)
        return SalesforceConnector.pool.get(username + password)

    @timer
    def query(self, query: str):
        query = self._prepare_query(query)
        logging.info('\n\n %s \n' % query)
        records = self.sf.query_all(query)
        return self._prepare_df(records['records'])

    @timer
    def query_all(self, query: str) -> pd.DataFrame:
        for i in range(10):
            try:
                query = self._prepare_query(query)
                logging.info('\n\n %s \n' % query)
                records = []
                headers = {'Authorization': 'Bearer ' + self.get_session()}
                response = requests.get(self.sf.base_url + 'queryAll/?q=' + query, headers=headers).json()
               ## logging.debug("\n\n Type of response  = {}".format(type(response)))
                ## logging.debug("Response received  = \n\n {}".format(response))
                records += response['records']
                while not response['done']:
                    logging.debug("nextRecordsUrl - " + response['nextRecordsUrl'])
                    response = requests.get(self.sf.base_url + 'query/' + response['nextRecordsUrl'].split('/')[-1],
                                            headers=headers).json()
                    records += response['records']
                # raise Exception
                return self._prepare_df(records)
            except Exception as e:
                if i == 9:
                    raise Exception("Unable To fetch isDeleted records from Salesforce. Contact service administrator.")
                logging.error("ERROR fetching isDeleted records from SF. Trying again")
                sleep(1)
                # continue


    # @timer
    # def bulk_download(self, object_name: str, query: str, filename=None) -> str:
    #     query = self._prepare_query(query)
    #     logging.info('\n\n %s \n' % query)
    #     query_job = self.bulk_sf.create_query_job(object_name=self._prepare_object_name(object_name), contentType='CSV', pk_chunking=250000)
    #     batch = self.bulk_sf.query(query_job, query)
    #     a = 0
    #     logging.debug("Waiting for batch to finish processing")
    #     sleep(2)
    #     while a <=0:
    #         l = []
    #         batch_list = self.bulk_sf.get_batch_list(query_job)
    #         for i in batch_list:
    #             l.append(i['state'])
    #         # logging.debug(set(l))
    #         if 'NotProcessed' in l and 'InProgress' not in l and 'Queued' not in l:
    #                 a = 1
    #         if a != 1:
    #             logging.debug("Waiting for batch to finish processing")
    #             sleep(5)
    #     logging.debug("Batch for %s complete." % object_name)
    #     logging.debug("No of internal batches created = {}".format(len(batch_list)))
    #     valid_batches = []
    #     for i in batch_list:
    #         if int(i['numberRecordsProcessed']) > 0:
    #             valid_batches.append(i)
    #     logging.debug("Length of Valid batches = {}".format(len(valid_batches)))
    #     if not filename:
    #         filename = object_name+str(uuid4())
    #     intermediate_csv_file = os.path.join('tmp', filename + '.csv')
    #     if valid_batches:
    #         with open(intermediate_csv_file, mode='a', encoding='utf-8') as f:
    #             for i in valid_batches:
    #                 logging.debug("Retrieving records for batch : {}".format(i['id']))
    #                 result        = self.bulk_sf.get_all_results_for_query_batch(i['id'],query_job)
    #                 result_byte   = b''.join(next(result))
    #                 result_string = result_byte.decode().replace('""','')
    #                 f.write(result_string)
    #         df = pd.read_csv(intermediate_csv_file,low_memory=False, header=None)
    #         df.drop_duplicates(subset = df.columns.tolist(), keep='first', inplace=True)
    #         df.columns = df.iloc[0]
    #         df.drop(df.index[0], inplace=True)
    #         df.to_csv(intermediate_csv_file, index = False)
    #     else:
    #         with open(intermediate_csv_file,'a') as f:
    #             f.write("Records not found for this query")
    #         logging.debug("Creating empty file")
    #     logging.debug("Write complete.")
    #     self.bulk_sf.close_job(query_job)
    #     return os.path.abspath(intermediate_csv_file)

    @timer
    def bulk_download(self, object_name: str, query: str, filename=None, pk_chunking=250000) -> str:
        query = self._prepare_query(query)
        logging.info('\n\n %s \n' % query)
        query_filter = query.split("FROM", maxsplit=1)[1]
        count_query = "SELECT count() FROM {} LIMIT 400000".format(query_filter)
        logging.debug("Count Query :- \n\n {} \n".format(count_query))
        try:
            count = self.sf.query(count_query)['totalSize']
            logging.debug("Count of valid records from query = {}".format(count))
        except Exception as e:
            logging.error("Count query timed out. Downloading with pk_chunking now.")
            logging.exception("ERROR")
            count = 400001
        if not filename:
            filename = object_name + "_" + datetime.now().strftime("%Y-%m-%d %H-%M-%S")
        intermediate_csv_file = os.path.join('tmp', filename + '.csv')
        if count:
            pk_chunking = False if count < 400000 else pk_chunking
            query_job = self.bulk_sf.create_query_job(object_name=self._prepare_object_name(object_name), contentType='CSV', 
                                                      pk_chunking=pk_chunking)
            batch = self.bulk_sf.query(query_job, query)
            if not pk_chunking:
                logging.debug("Downloading Without pk_chunking")
                self.bulk_sf.close_job(query_job)
                while not self.bulk_sf.is_batch_done(batch):
                    sleep(10)
                logging.debug("Batch for %s complete." % object_name)
                with open(intermediate_csv_file, mode='a', encoding='utf-8') as f:
                    for result in self.bulk_sf.get_all_results_for_query_batch(batch):
                        for each in result:
                            for attempt in range(BATCH_ATTEMPTS):
                                try:
                                    data = each.decode().replace('""', '')
                                    f.write(data)
                                    break
                                except (requests.RequestException, exceptions.SalesforceGeneralError) as err:
                                    logging.error("ERROR. Retrying after {} seconds".format(BATCH_SLEEP_TIME))
                                    sleep(BATCH_SLEEP_TIME)
                                    continue 
                logging.debug("Write complete.")
                return os.path.abspath(intermediate_csv_file)
            a = 0
            logging.debug("Waiting for batch to finish processing")
            sleep(5)
            while a <=0:
                l = []
                batch_list = self.bulk_sf.get_batch_list(query_job)
                for i in batch_list:
                    l.append(i['state'])
                # logging.debug(set(l))
                if 'NotProcessed' in l and 'InProgress' not in l and 'Queued' not in l:
                        a = 1
                if a != 1:
                    logging.debug("Waiting for batch to finish processing")
                    sleep(10)
            logging.debug("Batch for %s complete." % object_name)
            logging.debug("No of internal batches created = {}".format(len(batch_list)))
            valid_batches = []
            for i in batch_list:
                if int(i['numberRecordsProcessed']) > 0:
                    valid_batches.append(i)
            logging.debug("Length of Valid batches = {}".format(len(valid_batches)))
            lock = Lock()
            write_header = Value('i', 1)
            process_count = min(len(valid_batches), os.cpu_count()-2)
            processes = []
            start = 0
            for i in range(process_count):
                if i == process_count-1:
                    processes.append(Process(target=self.fetch_batch_data, args=(intermediate_csv_file, query_job, valid_batches[start:], 
                                                                                 lock, write_header)))
                else:
                    processes.append(Process(target=self.fetch_batch_data, args=(intermediate_csv_file, query_job, 
                                                                                 valid_batches[start:int(((i+1)*len(valid_batches))/(process_count))], 
                                                                                 lock, write_header)))
                    start = int(((i+1)*len(valid_batches))/(process_count))

            for p in processes:
                p.start()
            sleep(2)
            logging.debug("All processes started")
            for p in processes:
                p.join()
            logging.debug("All processes have finished")

            logging.debug("Write complete.")
            self.bulk_sf.close_job(query_job)
        else:
            logging.debug("Skipping querying of data as record count = 0")
            with open(intermediate_csv_file,'a') as f:
                    f.write("Records not found for this query")
            logging.debug("Empty file created")
        return os.path.abspath(intermediate_csv_file)

    def fetch_batch_data(self, intermediate_csv_file, query_job, batches, lock, write_header):
        # logging.debug("Fetching batch data by process {}".format(os.getpid()))
        # logging.debug("Batch List length = {}".format(len(batches)))
        for batch in batches:
            break_loop = False
            try:
                for timeout in range(TIMEOUT_ATTEMPTS):
                    if break_loop:
                        break
                    if timeout != 0:
                        logging.debug("Trying batch after {} minutes".format(BATCH_TIMEOUT/60))
                        sleep(BATCH_TIMEOUT)
                    for attempt in range(BATCH_ATTEMPTS):
                        try:
                            # logging.debug("Header Flag while fetching: {}".format(write_header.value))
                            logging.debug("Process {} Retrieving records for batch : {} ({} of {})".format(os.getpid(), batch['id'], batches.index(batch)+1, len(batches))) if attempt == 0 else ''
                            logging.info("Reattempting batch {} for the {} time".format(batch['id'], attempt + 1)) if attempt > 0 else ''
                            result        = self.bulk_sf.get_all_results_for_query_batch(batch['id'],query_job)
                            result_byte   = b''.join(next(result))
                            result_string = result_byte.decode().replace('""','')
                            lock.acquire()
                            # logging.debug("Header Flag while writing: {}".format(write_header.value))
                            if write_header.value == 1:
                                logging.debug("Writing batch data with header")
                                write_header.value = 0
                            else:
                                result_string = result_string[result_string.find('\n')+1:]
                            lock.release()
                            lock.acquire()
                            with open(intermediate_csv_file, mode='a', encoding='utf-8') as f:
                                f.write(result_string)
                            lock.release()
                            break_loop = True
                            break
                        except (requests.RequestException, exceptions.SalesforceGeneralError) as err:
                            logging.error("ERROR. Trying again after {} seconds".format(BATCH_SLEEP_TIME))
                            sleep(BATCH_SLEEP_TIME)
                            continue
            except Exception as e:
                logging.exception("ERROR")
                raise ValueError("Could not fetch batch from SF. Try again later")
            
    @timer
    def bulk_upload(self, object_name: str, df: pd.DataFrame):
        try:
            df = df.drop('Id', axis=1)
        except Exception as e:
            logging.warning("Skipped Id column deletion.")
        if not df.empty:
            return self.bulk_v2_functions("insert", object_name, df)

    @timer
    def bulk_delete(self, object_name: str, df):
        if isinstance(df, pd.Series):
            logging.warning('to be deleted records found series, converting to dataframe.')
            df = df.to_frame('Id')
        if not df.empty:
            return self.bulk_v2_functions("delete", object_name, df)

    @timer
    def bulk_update(self, object_name, df):
        if not df.empty:
            return self.bulk_v2_functions("update", object_name, df)

    def bulk_v2_functions(self, operation: str, object_name: str, df: pd.DataFrame):
        object_name = self._prepare_object_name(object_name)
        df.columns = df.columns.str.replace(self.namespace,"")
        df.columns = [self.namespace+x if x.endswith('__c') else x for x in df.columns.tolist()]
        filepath = os.path.abspath(os.path.join('tmp', object_name+str(uuid4())+ ".csv"))
        df.to_csv(filepath, index = False)
        fil = filepath
        FILESIZE = os.stat(filepath).st_size//(1024*1024)
        no_of_files = int(FILESIZE//100) + 1 if FILESIZE>100 else 1
        df = pd.read_csv(filepath, dtype = 'object')
        logging.debug("No of records = {}".format(len(df)))
        size = len(df)//no_of_files
        # logging.debug("No of Files = {}".format(no_of_files))
        jobs = []
        failed_records = []
        for i in range(no_of_files):
            logging.debug("Upload Iteration : {}".format(i+1))
            job_id = self.sf_v2.create_v2_job(operation, object_name)
            jobs.append(job_id)
            # logging.debug("Job_id: {}".format(job_id))
            filepath = filepath[:filepath.find(".csv")].replace("___{}".format(i-1),"") + "___"+str(i)+filepath[filepath.find(".csv"):]
            if i == no_of_files - 1:
                df.iloc[i*size:].to_csv(filepath,index = False)
            else: 
                df.iloc[i*size:(i+1)*size].to_csv(filepath, index = False)
            try:
                upload_response = self.sf_v2.upload_v2(job_id, filepath)
                # logging.debug(upload_response)
                if upload_response.status_code != 201:
                    raise requests.HTTPError("Malformed Request. HTTPError{}".format(upload_response.status_code))
                patch_response = self.sf_v2.patch_v2_job(job_id,"UploadComplete")
                if isinstance(patch_response,list):
                    logging.debug("Patch Failed")
                    logging.debug(patch_response)
                    raise ValueError
                # logging.debug(patch_response)
                self.sf_v2.wait_for_v2_job(job_id)
                status = self.sf_v2.get_v2_job_status(job_id)
                failed_records.append(status['numberRecordsFailed'])
                if int(status['numberRecordsFailed']) > 0:
                    logging.debug("Some records failed to {}.".format(operation))
                logging.debug(status)
                os.remove(filepath)
            except Exception as e:
                patch_response = self.sf_v2.patch_v2_job(job_id, "Aborted")
                logging.debug(e)
                raise e
                return("Exception Encountered")
        logging.debug("\n\n {} complete\n".format(operation))
        os.remove(fil)
        return zip(jobs, failed_records)

    def update(self, object_name, object_id, key, value):
        logging.debug(" %r %r " % (key, value))
        getattr(self.sf, self._prepare_object_name(object_name)).update(object_id, {self.namespace + key: value})

    def compound_update(self, object_name, object_id, values):
        values = self._prepare_keys(values)
        logging.debug(values)
        getattr(self.sf, self._prepare_object_name(object_name)).update(object_id, values)

    def get_session(self):
        return self.sf.session_id

    def get_apex_url(self):
        return self.sf.apex_url

    def get_pg_col_name(self, sfdc_col_name, column_details):
        return column_details.loc[
            (column_details['Source_Column__c'] == self._prepare_object_name(sfdc_col_name)),
            'tb_col_nm__c'].reset_index().loc[
            0, 'tb_col_nm__c']

    def update_column(self, df, column_name, column_value):
        df[self._prepare_object_name(column_name)] = column_value
        return df

    def _prepare_query(self, query: str) -> str:
        query = query.replace(self.namespace, "")
        object = query.split("FROM")[1].strip().split(" ")[0]
        if object in unmanaged_objects:
            # logging.debug("objects is not managed. No namespace at all in query")
            return query
        else:
            for custom_object in re.findall('( [0-9aA-zZ_]+__c| [0-9aA-zZ_]+__r)', query):
                if object in unmanaged_fields:
                    # logging.debug("Object is not completely managed. Checking for unmanaged fields")
                    if custom_object.strip() in unmanaged_fields[object]:
                        # logging.debug("Column {} is unmanaged. Skipping namespace attachment for it.".format(custom_object))
                        pass
                    else: 
                        query = query.replace(custom_object, ' ' + self._prepare_object_name(custom_object[1:]))
                else:
                    query = query.replace(custom_object, ' ' + self._prepare_object_name(custom_object[1:]))
            for custom_object in re.findall('(\.[0-9aA-zZ_]+__c)', query):
                query = query.replace(custom_object, '.' + self._prepare_object_name(custom_object[1:]))
            for custom_object in re.findall('(\.[0-9aA-zZ_]+__r)', query):
                query = query.replace(custom_object, '.' + self._prepare_object_name(custom_object[1:]))
        return query

    def _prepare_df(self, records):
        df = pd.DataFrame(records)
        try:
            df.columns = df.columns.str.replace(self.namespace, "")
            df = df.drop('attributes', axis=1)
        except Exception as e:
            logging.warning("Replacement of namespace failed or dropping attributes column.")
        logging.info("No. of records fetched %d" % len(df))
        return df

    def _prepare_keys(self, values):
        result = {}
        for key, value in values.items():
            result[self.namespace + key] = value
        return result

    def _prepare_object_name(self, object_name):
        if object_name.find(self.namespace) == -1 and (object_name.endswith('__c') or object_name.endswith('__r')):
            return self.namespace + object_name
        return object_name


class SalesForce_V2:
    def __init__(self, sf):
        self.sf = sf
        self.uploaded = False
        self.RecordsProcessed = None
        self.RecordsFailed = None
        self.wait_time = 0
        self.base_url = self.new_base_url()
        
    def new_base_url(self):
        return self.sf.base_url.replace(self.sf.sf_version,SF_VERSION_NEW)

    def create_v2_job(self, operation: str, object_name: str):
        logging.debug("Creating {} job".format(operation))
        headers     = {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + self.sf.session_id,
                        'X-Prettylogging.debug': '1'
                      }
        body        = {
                        "object" : object_name,
                        "contentType" : "CSV",
                        "operation" : operation
                      }
        result      = session.request("POST", self.base_url + BULK_V2_URI, headers=headers, 
                                      data=json.dumps(body))
        # response    = ast.literal_eval(result.content.decode("utf-8"))
        response    = result.json()
        job_id      = response["id"]
        return job_id
    
    def upload_v2(self, job_id: str, CSV_filepath: str):
        logging.debug("Uploading CSV to SFDC ...")
        headers     = {
                        'Content-Type': 'text/csv',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + self.sf.session_id,
                        'X-Prettylogging.debug': '1'
                      }
        body        = open(CSV_filepath, 'r').read()
        self.wait_time = max(1,len(pd.read_csv(CSV_filepath))/10000)
        result      = session.request("PUT", "{}{}{}/batches".format(self.base_url,BULK_V2_URI,job_id),
                                      headers=headers, data=body)
        self.uploaded = True
        return result
    
    def get_v2_job_status(self, job_id: str):
        logging.debug("Reading job status")
        headers     = {
                        'Content-Type': 'application/json;charset=UTF-8',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + self.sf.session_id,
                        'X-Prettylogging.debug': '1'
                      }
        result      = session.request("GET", "{}{}{}/".format(self.base_url,BULK_V2_URI,job_id), headers=headers)
        # response    = ast.literal_eval(result.content.decode("utf-8"))
        response    = result.json()
        logging.debug(response['state'])
        return response
    
    def patch_v2_job(self, job_id: str, patch_type: str):
        logging.debug("Pushing Job patch. {}".format(patch_type))
        if patch_type == "UploadComplete" and not self.uploaded:
            logging.debug("Upload CSV first")
            raise ValueError
        headers     = {
                        'Content-Type': 'application/json; charset=UTF-8',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + self.sf.session_id,
                        'X-Prettylogging.debug': '1'
                      }
        body        = {
                        'state':patch_type
                      }
        result      = session.request("PATCH", "{}{}{}/".format(self.base_url,BULK_V2_URI,job_id),
                                      headers=headers, data=json.dumps(body))
        # response    = ast.literal_eval(result.content.decode("utf-8"))
        response    = result.json()
        return response
    
    def wait_for_v2_job(self, job_id: str):
        logging.debug("Waiting for SFDC to process records")
        flag = True
        while flag:
            state = self.get_v2_job_status(job_id)["state"]
            if (state != 'JobComplete') and (state != 'Failed'):
                sleep((self.wait_time/2) + 0.25)
            else:
                flag = False
        return
    
    def fetch_v2_job_failed_records(self, job_id):
        logging.debug("Fetching Failed Records")
        headers     = {
                        'Content-Type': 'application/json;charset=UTF-8',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + self.sf.session_id,
                        'X-Prettylogging.debug': '1'
                      }
        result      = session.request("GET", "{}{}{}/failedResults".format(self.base_url,BULK_V2_URI,job_id),
                                      headers=headers)
        return result
    
    def fetch_v2_job_unprocessed(self, job_id):
        logging.debug("Fetching Unprocessed Records")
        headers     = {
                        'Content-Type': 'application/json;charset=UTF-8',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + self.sf.session_id,
                        'X-Prettylogging.debug': '1'
                      }
        result      = session.request("GET", "{}{}{}/unprocessedrecords".format(self.base_url,BULK_V2_URI,job_id),
                                      headers=headers)
        return result