import os
import logging

import psycopg2

from uuid import uuid4

from pandas import read_csv, DataFrame

from src.service.helpers import timer
from src.vendor.salesforce import SalesforceConnector


class Postgres:
    def __init__(self, connection_string: str):
        os.environ['PGOPTIONS'] = '-c statement_timeout=14400000' #statement_timeout value set to 4 hours
        self.conn = psycopg2.connect(connection_string)
        self.conn.autocommit = True

    @classmethod
    def initialise_from(cls, salesforce: SalesforceConnector):
        credentials = salesforce.query(
            """SELECT BR_PG_Database__c, BR_PG_Host__c, BR_PG_Password__c, BR_PG_Port__c, BR_PG_Schema__c, BR_PG_UserName__c FROM ETL_Config__c 
            WHERE Name = 'BRMS' LIMIT 1"""
        )
        connection_str = "dbname='{dbname}' host='{host}' port='{port}' user='{user}' password='{password}'".format(
            dbname=credentials.at[0, 'BR_PG_Database__c'],
            host=credentials.at[0, 'BR_PG_Host__c'],
            port=credentials.at[0, 'BR_PG_Port__c'],
            user=credentials.at[0, 'BR_PG_UserName__c'],
            password=credentials.at[0, 'BR_PG_Password__c']
        )
        return cls(connection_str)

    @timer
    def execute_query(self, query):
        logging.info('\n\n %s \n' % query)
        cur = self.conn.cursor()
        cur.execute(query)
        cur.close()

    @timer
    def download_data(self, query, column_details=DataFrame()):
        logging.info('\n\n {} \n'.format(query))
        abs_file_path = os.path.abspath(os.path.join('tmp', str(uuid4())) + '.csv')
        cur = self.conn.cursor()
        # cur.execute(query)
        # df = DataFrame(cur.fetchall())
        output_query = "Copy (" + query + ") TO '{}' WITH CSV HEADER".format(abs_file_path)
        # with open(abs_file_path, 'w') as f:
        #     cur.copy_expert(output_query, f)
        cur.execute(output_query)
        cur.close()
        df = read_csv(abs_file_path)
        logging.debug("Postgres record count %d" % len(df))
        if not column_details.empty:
            # df.columns = column_details["Source_Column__c"].values.tolist()
            df.columns = column_details[column_details['tb_col_nm__c'].isin(df.columns)]['Source_Column__c']
        df = df.apply(lambda x: x.fillna(""))
        # df.fillna("",inplace= True)
        return df

    @timer
    def return_output(self, query: str):
        logging.info('\n\n %s \n' % query)
        cur = self.conn.cursor()
        cur.execute(query)
        df = DataFrame(cur.fetchall())
        cur.close()
        return df
    
        
    def close(self):
        self.conn.close()


    def _in_tuple(self,iterable):
        return str(tuple(set(iterable))).replace(',)',')')

    def delete_records(self, table_name, key_column, key_values):
        cur = self.conn.cursor()
        query = "DELETE FROM "+table_name+" WHERE "+key_column+" IN "+_in_tuple(key_values)
        logging.debug('\n\n %s \n' % query)
        cur.execute(query)
        cur.close()

    def upload_from_csv(self, table_name, ordered_cols, file_name, delimiter=','):
        self.execute_query("COPY " + table_name + " (" + ordered_cols + ") FROM '" + file_name + "' DELIMITER '"+delimiter+"' CSV HEADER")
