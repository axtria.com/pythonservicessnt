import os
import logging
import requests
import simple_salesforce

from uuid import uuid4
from pandas import DataFrame, read_csv

from src.service.helpers import debugger
from src.vendor.postgres import Postgres
from src.service.register import register_job
from src.vendor.salesforce import SalesforceConnector

# FINAL_PROMOTE_TABLE = 't_final_promoted_'

@register_job("PromoteFromPostgres")
class PromoteFromPostgres:
    def __init__(self, do_promote: bool, dataset_id: str, scenario_id: str, user_name: str, password: str,
                 security_token: str, namespace: str, sandbox: bool, ip_addr: str, prefix: str):
        self.do_promote               = do_promote
        self.dataset_id               = None if dataset_id == '' else dataset_id
        self.scenario_id              = scenario_id
        self.namespace                = namespace
        self.user_name                = user_name
        self.password                 = password
        self.security_token           = security_token
        self.sandbox                  = sandbox
        self.salesforce               = SalesforceConnector.initialise_from_pool(user_name, password, security_token, namespace, sandbox)
        self.postgres                 = Postgres.initialise_from(self.salesforce)
        self.RRD                      = ''
        self.ip_addr                  = ip_addr
        self.prefix                   = prefix
        self.duplicated               = False

    def invoke(self):
        try:
            self.reestablish_connection()
            if self.is_scenario_valid():
                if not self.do_promote or not self.dataset_id:
                    logging.info("Skipping ... Postgres to SFDC ... Either do_promote false or no dataset id ")
                    self.salesforce.update('Scenario__c',self.scenario_id,'Rule_Execution_Status__c', "Rule Execution Completed")
                    self.mark_component()
                    if self.is_status_ready():
                        self.mark_success()
                    else:
                        self.RRD = "An individual component was either in progress or errored out"
                        self.mark_error()
                    return ' '
                if not self.is_status_ready():
                    logging.debug("Status not ready")
                    self.mark_component()
                    self.RRD = "An individual component was either in progress or errored out"
                    self.mark_error()
                    return ' '
                object_name = self.promote_to_salesforce()
                # if not object_name:
                #     self.mark_component()
                #     self.mark_error("Promote failed.")
                #     self.postgres.close()
                #     return ' '
                self.update_last_promote_success_date()
                self.mark_component()
                self.mark_success_for(object_name)
                self.postgres.close()
            else:
                self.RRD = "Scenario Rule_Execution_Status is False"
                logging.debug(self.RRD)
                self.mark_error(self.RRD)
                self.postgres.close()
                return self.RRD
            return ' '
        except Exception as e:
            logging.exception("Error")
            self.RRD = str(e)
            self.mark_error()
            self.postgres.close()
            return self.RRD

    def reestablish_connection(self):
        logging.debug("Checking if SF connection has timed out")
        try:
            self.salesforce.query("SELECT count() FROM Account LIMIT 1")
            logging.debug("Connection Ok. No Need to re-establish connection")
        except simple_salesforce.exceptions.SalesforceExpiredSession as e:
            logging.error("Invalid SessionError")
            self.salesforce = SalesforceConnector(self.user_name, self.password, self.security_token, 
                                                  self.namespace, self.sandbox)
            logging.info("SF Connection re-established")

    @debugger
    def is_scenario_valid(self):
        scenario_status = self.salesforce.query(
            """SELECT Rule_Execution_Status__c FROM Scenario__c WHERE Id = '{scenario_id}'
            """.format(scenario_id=self.scenario_id)).at[0, 'Rule_Execution_Status__c']
        return scenario_status != 'Error'

    def is_status_ready(self):
        query = "select  count(sfdc_id) from t_scn_rule_instance_details where status != 'Success' and scenario_id = '"+self.scenario_id+"'"
        df = self.postgres.return_output(query)
        count = df.iat[0,0]
        logging.debug('ID count from t_scn_rule_instance_details = {}'.format(count))
        query = ("select count(sfdc_id) from t_business_rule where "
                  "scenario_rule_instance_details in (SELECT sfdc_id FROM t_scn_rule_instance_details "
                  "where scenario_id = '{}') and status != 'Success'".format(self.scenario_id))
        df = self.postgres.return_output(query)
        count += df.at[0,0]
        logging.debug('ID count from both tables = {}'.format(count))

        if count != 0:
            # logging.info("Skipping ... postgres to sfdc ... at least one individual component not in 'success'")
            # self.mark_component()
            # self.mark_error("An individual component was either in progress or errored out")
            return  False
        return True

    def fetch_data_object(self):
        return self.salesforce.query("SELECT Data_Set_Object_Name__c "
                     "FROM Data_Set__c "
                     "WHERE Id = '{dataset_id}'".format(dataset_id=self.dataset_id))

    def fetch_column_details(self, dataset_id):
        return self.salesforce.query("SELECT Source_Column__c, tb_col_nm__c "
                                     "FROM Data_Set_Column_Detail__c "
                                     "WHERE dataset_id__c = '{dataset_id}' "
                                     "AND Source_Column__c != null "
                                     "AND (NOT Source_Column__c LIKE '%.%')"
                                     .format(dataset_id=dataset_id))

    def promote_to_salesforce(self):  
        logging.debug('Promoting...')
        data_object = self.fetch_data_object()
        object_name = data_object.at[0, 'Data_Set_Object_Name__c']
        # if object_name.lower() == (self.namespace + "position_account__c").lower():
        column_details = self.fetch_column_details(self.dataset_id)
        id_df = DataFrame({"Source_Column__c":['Id'],"tb_col_nm__c":['id']})
        column_details = column_details.append(id_df)

        operations = ['Delete', 'Update', 'Insert']
        for operation in operations:
            query = "SELECT " + ', '.join(column_details['tb_col_nm__c']) + " FROM {}{} WHERE flag = '{}'".format('t_final_promoted_',self.scenario_id, operation)
            records = self.postgres.download_data(query, column_details)
            if operation != 'Insert':
                if any(records["Id"].duplicated()):
                    self.duplicated = True
                    self.RRD = "duplicates found in data"
                    logging.debug(self.RRD)
            logging.debug("Fetched {} Records".format(operation))
            if operation == 'Insert':
                operation = 'upload'
            try:
                if operation == 'Delete':
                    records = records.loc[:,['Id']]
            except KeyError as e:
                logging.warning("Id column not present")
            response = getattr(self.salesforce,'bulk_' + operation.lower())(object_name,records)
            if response:
                for i in response:
                    logging.debug(i)
                    if int(i[1]) > 0:
                        logging.debug("Fetching failed records")
                        failures = self.salesforce.sf_v2.fetch_v2_job_failed_records(i[0])
                        file_name = "failed_records_" + str(uuid4()) + '.csv'
                        with open(os.path.join('promote_tmp', file_name), 'w') as f:
                            f.write(failures.text)
                        df = read_csv(os.path.join('promote_tmp', file_name))
                        self.RRD += df.at[0,"sf__Error"]
                        self.RRD += " Link for failed {} records:- https://{}{}/download/{}/promote_error_".format(operation, self.ip_addr, 
                                                                                                                 self.prefix, file_name)

                        # os.remove(os.path.join('promote_tmp', file_name))
                        break
                    elif self.duplicated:
                        self.duplicated = False
                        file_name = "duplicate_records_" + str(uuid4()) + '.csv'
                        records[records.duplicated(subset=['Id'], keep=False)].to_csv(os.path.join('promote_tmp', file_name), index=False)
                        self.RRD += " Link for duplicates in update records:- https://{}{}/download/{}/promote_error_".format(self.ip_addr, self.prefix, file_name)
                        break
            # failed_count = getattr(self.salesforce,'bulk_' + operation.lower())(object_name,records)
            # if failed_count:
            #     logging.debug("Some records failed to {} while promoting".format(operation))
            #     return ''
        return object_name

    def update_last_promote_success_date(self):
        df_response = self.salesforce.query(
            """SELECT Last_Run_Date__c FROM Scenario__c WHERE Id = '{scenario_id}'
            """.format(scenario_id=self.scenario_id)
        )
        last_prom_suc_date = df_response.at[0, 'Last_Run_Date__c']
        self.salesforce.update('Scenario__c', self.scenario_id, 'Last_Promote_Success_Date__c', last_prom_suc_date)

    def mark_error(self):
            self.salesforce.compound_update('Scenario__c', self.scenario_id, {
                'Rule_Execution_Status__c': 'Error',
                'Run_Response_Details__c': self.RRD
            })

    def mark_success(self):
        self.salesforce.compound_update('Scenario__c', self.scenario_id, {
            'Rule_Execution_Status__c': 'Success',
            'Run_Response_Details__c': self.RRD
        })

    def mark_success_for(self, object_name: str):
        logging.debug("Marking success")
        if object_name.lower() == (self.namespace + "position_account__c").lower():
            self.salesforce.compound_update('Scenario__c', self.scenario_id, {
                'Run_Response_Details__c': self.RRD
                })
            url_prefix = self.salesforce.get_apex_url()
            namespace_fix = self.namespace[:-2] + '/' if self.namespace else ''
            base_url = url_prefix + namespace_fix + "salesiq/reinitiateMetricsAndEsri?scenarioId="
            logging.info(base_url + self.scenario_id)
            headers = {'Authorization': 'Bearer ' + self.salesforce.get_session()}
            response = requests.get(base_url + self.scenario_id, headers=headers)
        else:
            self.mark_success()


    def mark_component(self):
        logging.debug("Marking components")
        query1 = "select  sfdc_id,scenario_id, status from t_scn_rule_instance_details where scenario_id = '{}'".format(self.scenario_id)
        query2 = ("select sfdc_id, scenario_rule_instance_details,status from t_business_rule where "
                  "scenario_rule_instance_details in (SELECT sfdc_id FROM t_scn_rule_instance_details "
                  "where scenario_id = '{}')".format(self.scenario_id))
        query3 = "select sfdc_id,scenario_id,status from t_config_call_balancing_map where scenario_id = '{}'".format(self.scenario_id)

        df1 = self.postgres.return_output(query1)
        df2 = self.postgres.return_output(query2)
        # df3 = self.postgres.return_output(query3)
        # self.flag = True

        # if df1[2].str.lower().any() != 'success':
        #     self.flag = False
        Scenario_Rule_Instance_Details_df = DataFrame()
        Scenario_Rule_Instance_Details_df['Id'] = df1[0]
        Scenario_Rule_Instance_Details_df['Scenario_Id__c'] = df1[1]
        Scenario_Rule_Instance_Details_df['Status__c'] = df1[2]
        # self.salesforce.bulk_update('Scenario_Rule_Instance_Details__c',Scenario_Rule_Instance_Details_df)
        Scenario_Rule_Instance_Details_df.columns = [self.namespace+i if i.endswith("__c") else i for i in Scenario_Rule_Instance_Details_df.columns]
        Scenario_Rule_Instance_Details_df.to_csv(os.path.join('tmp', "Scenario_Rule_Instance_Details__c_"+str(uuid4())+".csv"), index=False)
        logging.debug("Updating Scenario_Rule_Instance_Details__c")
        getattr(self.salesforce.sf.bulk, self.namespace + "Scenario_Rule_Instance_Details__c").update(Scenario_Rule_Instance_Details_df.to_dict(orient='records'))
        
        if not df2.empty:
            # if df2[2].str.lower().any() != 'success':
            #     self.flag = False
            Business_Rules_df = DataFrame()
            Business_Rules_df['Id'] = df2[0]
            Business_Rules_df['ScenarioRuleInstanceDetails__c'] = df2[1]
            Business_Rules_df['Status__c'] = df2[2]
            # self.salesforce.bulk_update('Business_Rules__c',Business_Rules_df)
            Business_Rules_df.columns = [self.namespace+i if i.endswith("__c") else i for i in Business_Rules_df.columns]
            Business_Rules_df.to_csv(os.path.join('tmp', "Business_Rules__c_"+str(uuid4()) + ".csv"), index=False)
            logging.debug("Updating Business_Rules__c")
            getattr(self.salesforce.sf.bulk, self.namespace + "Business_Rules__c").update(Business_Rules_df.to_dict(orient='records'))
