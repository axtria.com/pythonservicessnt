import os
import logging
import requests
import pandas as pd
import psycopg2 as ps
import simple_salesforce

from time import sleep
from datetime import datetime

from src.vendor.postgres import Postgres
from src.service.register import register_job
from src.vendor.salesforce import SalesforceConnector


@register_job("SfdcToPostgresMetadataCopy")
class SfdcToPostgresMetadataCopy:
    def __init__(self, scenario_id: str, user_name: str, password: str, security_token: str,
                 namespace: str, sandbox: bool):
        self.scenario_id = scenario_id
        self.namespace = namespace
        self.user_name = user_name
        self.password = password
        self.security_token = security_token
        self.sandbox = sandbox
        self.salesforce = SalesforceConnector.initialise_from_pool(user_name, password, security_token, namespace, sandbox)
        self.postgres = Postgres.initialise_from(self.salesforce)
        self.SF_PG_map_file = os.path.abspath('SFtoPGconfig.csv')
        self.PG_del_config  = os.path.abspath('delTablesConfigNew.csv')
        self.have_business_rules = True if self.get_business_rules_count() > 0 else False


    def invoke(self):
        try:
            for i in range(6):   #try to sync metadata a max of 5 times in case of connection failure
                logging.warning("Trying to sync metadata for the {} time".format(i+1)) if i > 0 else ''
                try:
                    if i == 5:
                        status = "Network Connectivity limited or server unavailable. Try again later"
                        logging.critical(status)
                        self.update_status(status)
                        self.postgres.close()
                        return status
    
                    self.prepare_scenario()
                    # raise ValueError("ValueError simulated")
                    # UPDATE
                    # self.update_scenario_rule_execution_status('Ready For Execution')
                    self.update_scenario_rule_instance_details('In Progress')
                    self.update_business_rules('In Progress')

                    #DELETE
                    self.delete_metadata_tables_from_postgres()
                    self.reestablish_connection()

                    #INSERT
                    self.move_metadata_sfdc_to_postgres()

                    #Execute scenario
                    # self.update_scenario_rule_execution_status('Rule Execution Initiated')           
                    # self.execute_scenario()
                    return ' '
                except (requests.RequestException, simple_salesforce.exceptions.SalesforceGeneralError) as e:
                    logging.exception("ERROR. Network Exception")
                    status = "Network Connectivity lost or server unavailable while syncing metadata. Waiting for 2 seconds before trying again"
                    logging.error(status)
                    sleep(2)
                    continue

                except ps.extensions.QueryCanceledError as p:
                    logging.critical("Postgres query has timed out. Exiting.")
                    self.reestablish_connection()
                    raise p

        except Exception as e:
            logging.exception("Error")
            self.reestablish_connection()
            self.update_status(e)
            self.postgres.close()
            return str(e)

    def prepare_scenario(self):
        if self.scenario_id:
            # self.scenario_start_datetime = self.current_datetime()
            self.salesforce.compound_update('Scenario__c', self.scenario_id, {
                                                                                'Rule_Execution_Status__c':'In Progress',
                                                                                'Run_Response_Details__c' : ''
                                                                             })


    def reestablish_connection(self):
        logging.debug("Checking if SF connection has timed out")
        try:
            self.salesforce.query("SELECT count() FROM Account LIMIT 1")
            logging.debug("Connection Ok. No Need to re-establish connection")
        except simple_salesforce.exceptions.SalesforceExpiredSession as e:
            logging.error("Invalid SessionError")
            self.salesforce = SalesforceConnector(self.user_name, self.password, self.security_token, 
                                                  self.namespace, self.sandbox)
            logging.info("SF Connection re-established")

    def get_business_rules_count(self):
        return self.salesforce.query(
            "SELECT Id FROM Business_Rules__c " 
            "WHERE ScenarioRuleInstanceDetails__r.Scenario_Id__c = '{scenario_id}'".format(scenario_id=self.scenario_id)).shape[0]

    def make_queries(self, mapping):
        mapping['SF_QUERY'] = 'SELECT ' + mapping['SF_COL'] + ' FROM ' + mapping['SF_TABLE']
        mapping.loc[~mapping['WHERE_CLAUSE'].isnull(),'SF_QUERY'] = mapping['SF_QUERY'] + ' WHERE ' + mapping['WHERE_CLAUSE']

        SRID_tuple = self._in_tuple(self.fetch_scenario_rule_instance_details_ids()['Id'])
        DSI_tuple  = self._in_tuple(self.fetch_data_set_ids(SRID_tuple)['dataset_id__c'])

        mapping['SF_QUERY'] = mapping['SF_QUERY'].str.replace('VAR_SCENARIO',repr(self.scenario_id))
        mapping['SF_QUERY'] = mapping['SF_QUERY'].str.replace('VAR_SRID',SRID_tuple)
        mapping['SF_QUERY'] = mapping['SF_QUERY'].str.replace('VAR_DSI' ,DSI_tuple )

        if self.have_business_rules:
            BR_tuple   = self._in_tuple(self.fetch_business_rules_ids()['Id'])
            mapping['SF_QUERY'] = mapping['SF_QUERY'].str.replace('VAR_BR', BR_tuple)
        else:
            mapping = mapping[~mapping['SF_QUERY'].str.contains('VAR_BR')]

        return mapping

    def fetch_scenario_rule_instance_details_ids(self):
         return self.salesforce.query(
            "SELECT Id FROM Scenario_Rule_Instance_Details__c " 
            "WHERE Scenario_Id__c = '{scenario_id}'".format(scenario_id=self.scenario_id))

    def fetch_business_rules_ids(self):
         return self.salesforce.query(
            "SELECT Id FROM Business_Rules__c " 
            "WHERE ScenarioRuleInstanceDetails__r.Scenario_Id__c = '{scenario_id}'".format(scenario_id=self.scenario_id))

    def fetch_data_set_ids(self,SRID_tuple):
         return self.salesforce.query(
            "SELECT dataset_id__c FROM Data_Set_Rule_Map__c " 
            "WHERE scenario_rule_instance_id__c IN " + SRID_tuple)   

    def update_status(self, e):
        self.salesforce.compound_update('Scenario__c', self.scenario_id, {
            'Rule_Execution_Status__c': 'Error',
            'Run_Response_Details__c': str(e)
        })    

    def update_scenario_rule_execution_status(self,value):
        self.salesforce.update('Scenario__c',self.scenario_id,'Rule_Execution_Status__c',value)

    def update_scenario_rule_instance_details(self,value):
        scenario_rule_instance_details = self.fetch_scenario_rule_instance_details_ids()
        scenario_rule_instance_details[self.namespace+'Status__c'] = value
        # self.salesforce.bulk_update('Scenario_Rule_Instance_Details__c',scenario_rule_instance_details)
        getattr(self.salesforce.sf.bulk, self.namespace+"Scenario_Rule_Instance_Details__c").update(scenario_rule_instance_details.to_dict(orient='records'))


    def update_business_rules(self,value):
        if self.have_business_rules:
            business_rules_details = self.fetch_business_rules_ids()
            business_rules_details[self.namespace+'Status__c'] = value
            # self.salesforce.bulk_update('Business_Rules__c',business_rules_details)
            getattr(self.salesforce.sf.bulk, self.namespace + "Business_Rules__c").update(business_rules_details.to_dict(orient='records'))

    def move_metadata_sfdc_to_postgres(self):
        mapping = pd.read_csv(self.SF_PG_map_file)
        mapping = mapping.dropna()
        queries = self.make_queries(mapping)
        output_abs_path = {}

        for i,sf in queries.iterrows():
            object_name   = sf['SF_TABLE']
            query         = sf['SF_QUERY']
            pg_table      = sf['PG_TABLE']
            out_file_name = datetime.now().strftime('%Y%m%d_%H%M%S') + '__'+ self.scenario_id + '__' + object_name + '__TO__' + pg_table
            
            if query.find("__r.") == -1:
                df = self.salesforce.query(query)
                abs_path = os.path.abspath(os.path.join('tmp', out_file_name + '.csv'))
                if not df.empty:
                    df.to_csv(abs_path, index=False)
                else:
                    with open(abs_path, 'a') as f:
                        f.write("Records not found for this query.")
            else:
                abs_path      = self.salesforce.bulk_download(object_name, query, out_file_name, pk_chunking = False)
            output_abs_path[pg_table] = abs_path

        debug_table = pd.DataFrame(columns=['pg_table','sf_records_count','pre_count','post_count']) ##
        for i,pg in queries.iterrows():
            pg_table = pg['PG_TABLE']
            pg_cols  = pg['PG_COL']
            csv_path = output_abs_path[pg_table]

            sf_records_count = pd.read_csv(csv_path).shape[0] ##
            pre_count = self.postgres.return_output('select count(*) from '+pg_table)[0][0] ##
            self.postgres.upload_from_csv(pg_table, pg_cols, csv_path)
            post_count = self.postgres.return_output('select count(*) from '+pg_table)[0][0] ##
            debug_table.loc[len(debug_table)] = {'pg_table':pg_table,'sf_records_count':sf_records_count,'pre_count':pre_count,'post_count':post_count} ##
        fname = os.path.abspath(os.path.join('tmp', 'debug_table'+ datetime.now().strftime('%Y%m%d_%H%M%S') + '.csv')) ##
        debug_table.to_csv(fname) ##


    def delete_metadata_tables_from_postgres(self):
        self.postgres.execute_query("select public.fn_delete_scenario_data_dynamic("+repr(self.PG_del_config)+","+repr(self.scenario_id)+")")

    def execute_scenario(self):
        self.postgres.execute_query("select public.fn_execute_scenario("+repr(self.scenario_id)+",'Run')")

    def _in_tuple(self,iterable):
        return str(tuple(set(iterable))).replace(',)',')')

