import traceback
import logging

from pandas import DataFrame, read_csv, merge

from src.service.register import register_job
from src.vendor.postgres import Postgres
from src.vendor.salesforce import SalesforceConnector


@register_job("SfdcToPostgres")
class SfdcToPostgres:
    def __init__(self, data_object_ids: str, scenario_id: str, user_name: str, password: str, security_token: str,
                 namespace: str, sandbox: bool):
        self.data_object_ids = filter(lambda x: x, data_object_ids.split(','))
        self.scenario_id = scenario_id
        self.salesforce = SalesforceConnector.initialise_from_pool(user_name, password, security_token, namespace, sandbox)
        self.postgres = Postgres.initialise_from(self.salesforce)

    def invoke(self):
        self.prepare_scenario()

        for data_object_id in self.data_object_ids:
            try:
                data_object = self.fetch_data_object(data_object_id)
                # Attributes derived from data object
                dataset_id = data_object.loc[0, 'dataset_id__c']
                object_name = data_object.loc[0, 'Data_Set_Object_Name__c']
                table_name = data_object.loc[0, 'Final_Table__c']

                raw_column_details = self.fetch_column_details(dataset_id)
                column_details = raw_column_details.dropna(axis=0)
                self.create_postgres_table(table_name, raw_column_details)

                filter_for_sfdc, filter_for_postgres = self.generate_filter_condition(column_details)
                sfdc_filename = self.fetch_sfdc_records(object_name, column_details, filter_for_sfdc)
                self.copy_to_database(sfdc_filename, table_name, column_details, filter_for_postgres)

            except Exception as e:
                logging.error(e)
                logging.error(traceback.format_exc())
                self.update_sfdc_status(e)

    def fetch_data_object(self, data_object_id):
        return self.salesforce.query(
            "SELECT dataset_id__c, Data_Set_Object_Name__c, Final_Table__c "
            "FROM Data_Object__c "
            "WHERE Id = '{data_object_id}'".format(data_object_id=data_object_id))

    def fetch_column_details(self, dataset_id):
        return self.salesforce.query(
            "SELECT Source_Column__c, tb_col_nm__c, datatype__c, Table_Data_Type__c "
            "FROM Data_Set_Column_Detail__c "
            "WHERE dataset_id__c = '{dataset_id}'".format(dataset_id=dataset_id)
        )

    def prepare_scenario(self):
        # No longer in use.
        # if self.scenario_id:
        #     self.salesforce.update('Scenario__c', self.scenario_id, 'Rule_Execution_Status__c', 'In Progress')
        pass

    def generate_filter_condition(self, column_details):
        if self.scenario_id:
            team_instance = self.salesforce.query(
                "SELECT Team_Instance__c FROM Scenario__c "
                "WHERE Id = '{scenario_id}'".format(scenario_id=self.scenario_id))

            team_instance_col_name = self.salesforce.get_pg_col_name('Team_Instance__c', column_details)

            filter_for_sfdc = " WHERE Team_Instance__c = '" + team_instance.loc[0, 'Team_Instance__c'] + "'"
            filter_for_postgres = " WHERE " + team_instance_col_name + "= '" + team_instance.loc[
                0, 'Team_Instance__c'] + "'"

            return filter_for_sfdc, filter_for_postgres
        return '', ''

    def fetch_sfdc_records(self, object_name, column_details, filter_str):
        column_details_str = ', '.join(column_details['Source_Column__c'])

        bulk_query = "SELECT " + column_details_str + " FROM " + object_name + filter_str
        return self.salesforce.bulk_download(object_name, bulk_query)

    def create_postgres_table(self, table_name: str, column_details: DataFrame):
        schema = '( '
        for index, row in column_details.iterrows():
            if (row["Table_Data_Type__c"].lower() != "text"):
                schema += row["tb_col_nm__c"] + " " + row["Table_Data_Type__c"].lower() + ", "
            else:
                schema += row["tb_col_nm__c"] + " " + "varchar, "
        schema = schema[:-2] + ' )'
        create_table_query = "create table IF NOT EXISTS {} {}".format(table_name, schema)
        self.postgres.execute_query(create_table_query)

    def copy_to_database(self, filename, table_name, column_details, filter_str):
        df = read_csv(filename)
        column_details['Source_Column__c'] = column_details['Source_Column__c'].str.lower()
        self.postgres.execute_query("DELETE FROM " + table_name + filter_str)
        if self._is_valid(df):
            actual_columns = DataFrame(list(df.columns.str.lower()), columns=['actual_order'])
            ordered_pg_columns = merge(actual_columns, column_details, left_on='actual_order',
                                       right_on='Source_Column__c', how='left')['tb_col_nm__c']
            self.postgres.execute_query("COPY " + table_name +
                                        "(" + ', '.join(ordered_pg_columns) +
                                        ") FROM '" + filename + "' DELIMITER ',' CSV HEADER")

    def update_sfdc_status(self, e):
        if self.scenario_id:
            logging.warning("Marking error for scenario rule_execution_status")
            self.salesforce.compound_update('Scenario__c', self.scenario_id, {
                'Rule_Execution_Status__c': 'Error',
                'Run_Response_Details__c': str(e)
                })
            
    def _is_valid(self, df: DataFrame):
        if len(df) > 0:
            logging.debug(" No. of records - %d" %len(df))
            return True
        else:
            logging.warning(" No. of records found to be 0.")
            return False
