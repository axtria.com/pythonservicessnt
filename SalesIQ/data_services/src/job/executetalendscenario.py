import logging

import requests
from requests import HTTPError

from src.service.helpers import debugger
from src.service.register import register_job
from src.vendor.salesforce import SalesforceConnector


@register_job("ExecuteTalendScenario")
class ExecuteTalendScenario:
    def __init__(self, scenario_id: str, user_name: str, password: str, security_token: str, namespace: str, sandbox: bool):
        self.scenario_id = scenario_id
        self.namespace = namespace
        self.sandbox = sandbox
        self.salesforce = SalesforceConnector.initialise_from_pool(user_name, password, security_token, namespace, sandbox)

    def invoke(self):
        try:
            if self.is_scenario_valid():
            # if 1 == 1:
                etl_config = self.fetch_etl_config()

                base_url = etl_config.at[0,'End_Point__c']

                params = '&arg0=--context_param scenarioParam=' + self.scenario_id + '~Debug' + \
                         '&arg1=--context_param s_username=' + etl_config.at[0, 'SF_UserName__c'] + \
                         '&arg2=--context_param s_password=' + etl_config.at[0, 'SF_Password__c'] + \
                         etl_config.at[0, 'S3_Security_Token__c'] + \
                         '&arg3=--context_param pg_host=' + etl_config.at[0, 'BR_PG_Host__c'] + \
                         '&arg4=--context_param pg_username=' + etl_config.at[0, 'BR_PG_UserName__c'] + \
                         '&arg5=--context_param pg_port=' + etl_config.at[0, 'BR_PG_Port__c'] + \
                         '&arg6=--context_param pg_password=' + etl_config.at[0, 'BR_PG_Password__c'] + \
                         '&arg7=--context_param pg_database=' + etl_config.at[0, 'BR_PG_Database__c'] + \
                         '&arg8=--context_param s3_folder_name=' + etl_config.at[0, 'S3_Filename__c'] + \
                         '&arg9=--context_param access_key=' + etl_config.at[0, 'S3_Access_Key__c'] + \
                         '&arg10=--context_param secret_key=' + etl_config.at[0, 'S3_Key_Name__c'] + \
                         '&arg11=--context_param ftp_host=' + etl_config.at[0, 'SFTP_Host__c'] + \
                         '&arg12=--context_param ftp_password=' + etl_config.at[0, 'SFTP_Password__c'] + \
                         '&arg13=--context_param ftp_username=' + etl_config.at[0, 'SFTP_Username__c'] + \
                         '&arg14=--context_param ftp_folder=' + etl_config.at[0, 'SFTP_Folder__c'] + \
                         '&arg15=--context_param is_sandbox=' + str(self.sandbox)


                logging.info(base_url + params)
                response = requests.get(base_url + params)
                if response.status_code == 200:
                    logging.info("Talend Job call success.")
                else:
                    logging.warning(response.text)
                    raise HTTPError
            else:
                self.update_sfdc_status("Did Not start talend call as scenario_status was marked as error.")
                return "Did Not start talend call as scenario_status was marked as error."
            return ' '
        except Exception as e:
            logging.exception("Error")
            self.update_sfdc_status("Unable to call ExecuteTalendScenario job")
            return str(e)

    def fetch_etl_config(self):
        return self.salesforce.query(
            """SELECT BR_PG_Database__c, BR_PG_Host__c, BR_PG_Password__c,
             BR_PG_Port__c, BR_PG_Schema__c, BR_PG_UserName__c, 
             S3_Access_Key__c, S3_Key_Name__c, S3_Filename__c, S3_Security_Token__c, 
             SFTP_Folder__c, SFTP_Host__c, SFTP_Password__c, SFTP_Port__c, 
             SFTP_Username__c, SF_Password__c, SF_UserName__c, End_Point__c 
            FROM ETL_Config__c 
            WHERE Name = 'BRMS' LIMIT 1""")

    @debugger
    def is_scenario_valid(self) -> bool:
        scenario_status = self.salesforce.query(
            """SELECT Rule_Execution_Status__c FROM Scenario__c WHERE Id = '{scenario_id}'
            """.format(scenario_id=self.scenario_id)).at[0, 'Rule_Execution_Status__c']

        return scenario_status != 'Error'

    def update_sfdc_status(self, e):
        self.salesforce.compound_update('Scenario__c', self.scenario_id, {
            'Rule_Execution_Status__c': 'Error',
            'Run_Response_Details__c': str(e) 
            })
