import logging
from datetime import datetime, timedelta
from pandas import DataFrame

import requests

from src.service.helpers import debugger
from src.service.register import register_job
from src.vendor.postgres import Postgres
from src.vendor.salesforce import SalesforceConnector


# @register_job("DeltaPostgresToSfdc")
class DeltaPostgresToSfdc:
    def __init__(self, do_promote: bool, dataset_id: str, scenario_id: str, user_name: str, password: str,
                 security_token: str, table_name: str, namespace: str, is_delta: str, sandbox: bool,
                 compare_table_name: str, compare_table_filter: str, compare_dataset_id: str,
                 customer_univ_table_name: str, customer_univ_dataset_id: str):
        self.do_promote = do_promote
        self.dataset_id = None if dataset_id == '' else dataset_id
        self.scenario_id = scenario_id
        self.table_name = table_name
        self.is_delta = is_delta
        self.compare_table_name = compare_table_name
        self.namespace = namespace
        self.compare_table_filter = " WHERE " + compare_table_filter if compare_table_filter else compare_table_filter
        self.compare_dataset_id = compare_dataset_id
        self.customer_univ_table_name = customer_univ_table_name
        self.customer_univ_dataset_id = customer_univ_dataset_id
        self.salesforce = SalesforceConnector.initialise_from_pool(user_name, password, security_token, namespace,
                                                                   sandbox)
        self.postgres = Postgres.initialise_from(self.salesforce)
        self.flag = ''

    def invoke(self):
        try:
            if self.is_scenario_valid():
            # if 1 == 1:

                if not self.do_promote or not self.dataset_id:
                    logging.info("Skipping ... Postgres to SFDC ... Either do_promote false or no dataset id ")
                    self.salesforce.update('Scenario__c',self.scenario_id,'Rule_Execution_Status__c', "Rule Execution Completed")
                    self.mark_component()
                    if self.flag:
                        self.mark_success()
                    else:
                        self.update_status("An individual component was either in progress or errored out")
                    return ' '
                self.salesforce.update('Scenario__c',self.scenario_id,'Rule_Execution_Status__c', "Rule Execution Completed and Promote Started")
                if not self.is_status_ready():
                    logging.debug("Status not ready")
                    self.mark_component()
                    self.update_status("An individual component was either in progress or errored out")
                    return ' '

                logging.debug('promoting...')
                data_object = self.fetch_data_object()
                object_name = data_object.loc[0, 'Data_Set_Object_Name__c']
                filter_for_sfdc, filter_for_postgres = self.generate_filter_condition()                

                if object_name.lower() == (self.namespace + "position_account__c").lower():
                    column_details = self.fetch_column_details(self.dataset_id)
                    team_instance = self.fetch_team_instance()  
                    # Attributes
                    alignment_period = team_instance.at[0, 'Alignment_Period__c'].lower()

                    getattr(self, alignment_period + "_" + self.is_delta)(team_instance, object_name, column_details,
                                                                      filter_for_sfdc, filter_for_postgres)
                elif object_name.lower() == (self.namespace + "account_exclusion__c").lower():
                    column_details = self.fetch_column_details(self.customer_univ_dataset_id)
                    column_details_prod = self.fetch_column_details(self.dataset_id)
                    if self.is_delta == "false":
                        query = "SELECT Id from Account_Exclusion__c WHERE Block_Type__c "\
                                "= 'Implicit' and Status__c = 'Active'"
                        records = self.salesforce.query(query)

                        records[self.namespace + "Status__c"] = "Inactive"
                        self.salesforce.bulk_update("Account_Exclusion__c", records)
                        download_query =  "SELECT " + ", ".join(column_details_prod["tb_col_nm__c"]) + " from {}".format(self.table_name)
                        df = self.postgres.download_data(download_query,column_details_prod)
                        self.salesforce.bulk_upload(object_name, df)
                    else:
                        tb_col_nm_id = self.salesforce.get_pg_col_name('Id', column_details) # column_details.loc[column_details["Source_Column__c"] == "Id", "tb_col_nm__c"]
                        distinct_val_query = "SELECT distinct {col} FROM {table}".format(col = tb_col_nm_id,
                                                                                        table = self.customer_univ_table_name)
                        distinct_df       = self.postgres.download_data(distinct_val_query)
                        impl_active_acc   = "SELECT Id from Account_Exclusion__c WHERE Block_Type__c = 'Implicit' AND Status__c = 'Active'"                                       
                        # logging.info("distinct Dataframe")
                        # logging.info(distinct_df)
                        if not distinct_df.empty:
                            impl_active_acc += " AND ("
                            for pg_id in distinct_df[tb_col_nm_id]:
                                impl_active_acc += " Account__c = '{}' OR ".format(pg_id)
                            impl_active_acc = impl_active_acc[:-4] + ")"
                        inactive_df     = self.salesforce.query(impl_active_acc)
                        inactive_df[self.namespace + "Status__c"] = 'Inactive'
                        self.salesforce.bulk_update(object_name, inactive_df)
                        download_query =  "SELECT " + ", ".join(column_details_prod["tb_col_nm__c"]) + " from {}".format(self.table_name)
                        df = self.postgres.download_data(download_query,column_details_prod)
                        self.salesforce.bulk_upload(object_name, df)
                
                self.update_last_promote_success_date()
                self.mark_component()
                # if self.scenario:
                self.mark_success_for(object_name)
                # else:
                #     self.update_status("An individual component was either in progress or errored out")
                self.postgres.close()
            else:
                v = "Scenario Rule_Execution_Status is False. If this service was run as a part of run full" \
                    " and promote, then talend marked error. Else change status to empty or ready before" \
                    " calling this service independently."
                logging.debug(v)
                return v
            return ' '
        except Exception as e:
            logging.exception("Error")
            self.update_status(e)
            self.postgres.close()
            return str(e)

    def is_status_ready(self):
        query = "select  count(sfdc_id) from t_scn_rule_instance_details where status in ('Error Occured', 'In Progress') and scenario_id = '"+self.scenario_id+"'"
        df = self.postgres.return_output(query)
        count = df.iat[0,0]
        logging.debug('ID count = {}'.format(count))
        if count != 0:
            logging.info("Skipping ... postgres to sfdc ... at least one individual component not in 'success'")
            # self.mark_component()
            self.update_status("An individual component was either in progress or errored out")
            return  False
        return True

    def fetch_team_instance(self):
        return self.salesforce.query(
            """SELECT IC_EffstartDate__c, IC_EffEndDate__c, Alignment_Period__c 
            FROM Team_Instance__c 
            WHERE Scenario__c = '{scenario_id}'
            """.format(scenario_id=self.scenario_id))

    def fetch_column_details(self, dataset_id):
        return self.salesforce.query("SELECT Source_Column__c, tb_col_nm__c "
                                     "FROM Data_Set_Column_Detail__c "
                                     "WHERE dataset_id__c = '{dataset_id}' "
                                     "AND Source_Column__c != null "
                                     "AND (NOT Source_Column__c LIKE '%.%')"
                                     .format(dataset_id=dataset_id))

    def fetch_data_object(self):
        return self.salesforce.query("SELECT Data_Set_Object_Name__c "
                                     "FROM Data_Set__c "
                                     "WHERE Id = '{dataset_id}'".format(dataset_id=self.dataset_id))

    @debugger
    def is_scenario_valid(self):
        scenario_status = self.salesforce.query(
            """SELECT Rule_Execution_Status__c FROM Scenario__c WHERE Id = '{scenario_id}'
            """.format(scenario_id=self.scenario_id)).at[0, 'Rule_Execution_Status__c']
        return scenario_status != 'Error'

    def generate_filter_condition(self):
        query = """SELECT Filter_Expression_Destination__c, Filter_Expression_Source__c
        FROM Scenario_Data_Object_Map__c WHERE Data_set__c = '{dataset_id}'
        AND Scenario__c = '{scenario_id}'""".format(dataset_id=self.dataset_id,
                                                    scenario_id=self.scenario_id)

        df_null_check = self.salesforce.query(query)
        if not df_null_check.at[0, 'Filter_Expression_Source__c']:
            raise ValueError
        filter_for_sfdc = " WHERE " + df_null_check.at[0, 'Filter_Expression_Source__c']
        filter_for_postgres = " WHERE " + df_null_check.at[0, 'Filter_Expression_Destination__c']
        return filter_for_sfdc, filter_for_postgres

    def update_last_promote_success_date(self):
        df_response = self.salesforce.query(
            """SELECT Last_Run_Date__c FROM Scenario__c WHERE Id = '{scenario_id}'
            """.format(scenario_id=self.scenario_id)
        )
        last_prom_suc_date = df_response.at[0, 'Last_Run_Date__c']
        self.salesforce.update('Scenario__c', self.scenario_id, 'Last_Promote_Success_Date__c', last_prom_suc_date)

    def update_status(self, e):
        if not self.flag:
            self.salesforce.compound_update('Scenario__c', self.scenario_id, {
                'Rule_Execution_Status__c': 'Error',
                'Run_Response_Details__c': str(e)
            })

    def mark_success(self):
        self.salesforce.compound_update('Scenario__c', self.scenario_id, {
            'Rule_Execution_Status__c': 'Success',
            'Run_Response_Details__c': ''
        })

    def mark_error(self):
        self.salesforce.compound_update('Scenario__c', self.scenario_id, {
            'Rule_Execution_Status__c': 'Error',
            'Run_Response_Details__c': ''
        })       

    def mark_success_for(self, object_name: str):
        logging.debug("Marking success")
        if object_name.lower() == (self.namespace + "position_account__c").lower():
            url_prefix = self.salesforce.get_apex_url()
            namespace_fix = self.namespace[:-2] + '/' if self.namespace else ''
            base_url = url_prefix + namespace_fix + "salesiq/reinitiateMetricsAndEsri?scenarioId="
            logging.info(base_url + self.scenario_id)
            headers = {'Authorization': 'Bearer ' + self.salesforce.get_session()}
            response = requests.get(base_url + self.scenario_id, headers=headers)
        else:
            self.mark_success()
        

    def current_true(self, team_instance, object_name, column_details, filter_for_sfdc, filter_for_postgres):
        #
        # Active Implicit
        #
        dataset_column_detail = self.fetch_column_details(self.compare_dataset_id)
        pg_id = self.salesforce.get_pg_col_name('Id', dataset_column_detail)
        account = self.salesforce.get_pg_col_name('Account__c', dataset_column_detail)
        position = self.salesforce.get_pg_col_name('Position__c', dataset_column_detail)
        assignment_status = self.salesforce.get_pg_col_name('Assignment_Status__c', dataset_column_detail)
        affiliation_based = self.salesforce.get_pg_col_name('Affiliation_Based_Alignment__c', dataset_column_detail)
        account_alignment_type = self.salesforce.get_pg_col_name('Account_Alignment_Type__c', dataset_column_detail)

        filter_active_query = "SELECT {pg_id}, {account} FROM " + \
                              self.compare_table_name + self.compare_table_filter + """ 
                                              AND {assignment_status} = 'Active' 
                                              AND ({account_alignment_type} = 'Implicit' 
                                              OR ({account_alignment_type} = 'Explicit' 
                                              AND lower({affiliation_based}) = 'true') ) """
        filter_active_query = filter_active_query.format(pg_id=pg_id, account=account,
                                                         account_alignment_type=account_alignment_type,
                                                         assignment_status=assignment_status,
                                                         affiliation_based=affiliation_based)
        active_implicit = self.postgres.download_data(filter_active_query)

        #
        # Future Active
        #
        future_active_query = " SELECT {pg_id}, {account} FROM " + self.compare_table_name + \
                              self.compare_table_filter + " AND {assignment_status} = 'Future Active' "
        future_active_query = future_active_query.format(assignment_status=assignment_status, pg_id=pg_id,
                                                         account=account)
        future_active = self.postgres.download_data(future_active_query)

        #
        # Explicits - SFDC - compare table
        #
        sf_explicit_query = "SELECT {pg_id}, {position}, {account} FROM " + \
                            self.compare_table_name + self.compare_table_filter + \
                            " AND {assignment_status} = 'Active' AND ({account_alignment_type} = 'Explicit'" \
                            " AND ({affiliation_based} is NULL OR lower({affiliation_based}) = 'false'))"
        sf_explicit = self.postgres.download_data(sf_explicit_query.format(
            pg_id=pg_id, position=position, account=account, assignment_status=assignment_status,
            account_alignment_type=account_alignment_type, affiliation_based=affiliation_based))

        #
        # Customer universe
        #
        cust_column_detail = self.fetch_column_details(self.customer_univ_dataset_id)
        cust_pg_id = self.salesforce.get_pg_col_name('Id', cust_column_detail)
        cust_to_be_updated_query = "SELECT {id} FROM {table_name} ".format(id=cust_pg_id,
                                                                           table_name=self.customer_univ_table_name)
        cust_to_be_updated = self.postgres.download_data(cust_to_be_updated_query)

        #
        # Update - active implicit [Id, Effective_End_Date__c]
        #
        active_implicit = active_implicit[active_implicit[account].isin(cust_to_be_updated[cust_pg_id])][[pg_id]]
        active_implicit.rename(columns={pg_id: 'Id'}, inplace=True)
        active_implicit = self.salesforce.update_column(active_implicit, 'Effective_End_Date__c',
                                                        (datetime.today() - timedelta(days=1)).strftime("%Y-%m-%d"))
        self.salesforce.bulk_update(object_name, active_implicit)

        #
        # Delete - Future Active [Id]
        #
        future_active = future_active[future_active[account].isin(cust_to_be_updated[cust_pg_id])].loc[:, pg_id]
        self.salesforce.bulk_delete(object_name, future_active)

        # -----------------------------------------------------------------------------------
        # Upload - upstream [ All columns ]
        #
        upstream_account = self.salesforce.get_pg_col_name('Account__c', column_details)
        upstream_position = self.salesforce.get_pg_col_name('Position__c', column_details)
        upstream_account_alignment_type = self.salesforce.get_pg_col_name('Account_Alignment_Type__c', column_details)
        upstream_affiliation_based = self.salesforce.get_pg_col_name('Affiliation_Based_Alignment__c', column_details)

        filter_query = filter_for_postgres + """  AND ({account_alignment_type} = 'Implicit' 
                OR ({account_alignment_type} = 'Explicit' AND lower({affiliation_based}) = 'true'))""".format(
            account_alignment_type=upstream_account_alignment_type, affiliation_based=upstream_affiliation_based)
        upstream_query = "SELECT " + ', '.join(column_details['tb_col_nm__c']) + \
                         " FROM " + self.table_name + filter_query + " ORDER BY " + upstream_account
        upstream_df = self.postgres.download_data(upstream_query, column_details)

        self.salesforce.update_column(upstream_df, 'Effective_Start_Date__c', datetime.today().strftime("%Y-%m-%d"))
        self.salesforce.update_column(upstream_df, 'Effective_End_Date__c', team_instance.at[0, 'IC_EffEndDate__c'])
        self.salesforce.bulk_upload(object_name, upstream_df)

        #
        # Explicits - Postgres
        #
        pg_explicit_query = "SELECT " + ', '.join(column_details['tb_col_nm__c']) + \
                            " FROM " + self.table_name + filter_for_postgres + \
                            " AND ({account_alignment_type} = 'Explicit' " + " AND ({affiliation_based} is NULL " + \
                            " OR lower({affiliation_based}) = 'false')) ORDER BY " + upstream_account
        pg_explicit_query = pg_explicit_query.format(account_alignment_type=upstream_account_alignment_type,
                                                     affiliation_based=upstream_affiliation_based)
        pg_explicit = self.postgres.download_data(pg_explicit_query)

        # End date [Id, Effective_End_Date__c]
        logging.debug("\n\nSf_explicit count = {}".format(len(sf_explicit)))
        logging.debug("\n {}".format(sf_explicit.head()))
        sf_explicit_not_found = sf_explicit[~((sf_explicit[position].str.strip() + sf_explicit[account].str.strip()).isin(
            pg_explicit[upstream_position].str.strip() + pg_explicit[upstream_account].str.strip()))][[pg_id]]
        sf_explicit_not_found.rename(columns={pg_id: 'Id'}, inplace=True)
        logging.debug("\n\nSf_explicit not found count = {}".format(len(sf_explicit_not_found)))
        logging.debug("\n {}".format(sf_explicit_not_found.head()))
        logging.debug("\n {}".format(pg_explicit.head()))
        sf_explicit_not_found = self.salesforce.update_column(sf_explicit_not_found, 'Effective_End_Date__c',
                                                              (datetime.today() - timedelta(days=1)).strftime(
                                                                  "%Y-%m-%d"))
        logging.debug("Marked sf_explicit not found's end date as yesterday's date")

        self.salesforce.bulk_update(object_name, sf_explicit_not_found)
        logging.debug("Bulk Updated {}".format(object_name))
        #
        # Insert records - upstream table name [All columns]
        #
        pg_explicit_not_found = pg_explicit[~((pg_explicit[upstream_position] + pg_explicit[upstream_account]).isin(
            sf_explicit[position] + sf_explicit[account]))]
        pg_explicit_not_found.columns = column_details['Source_Column__c']
        self.salesforce.bulk_upload(object_name, pg_explicit_not_found)

    def current_false(self, team_instance, object_name, column_details, filter_for_sfdc, filter_for_postgres):
        logging.debug("inside current false")
        # end date Active records in SFDC
        to_be_updated = self.salesforce.query(
            "SELECT Id FROM {object_name} {filter_condition}".format(
                object_name=object_name,
                filter_condition=filter_for_sfdc + " AND Assignment_Status__c = 'Active' "))
        to_be_updated = self.salesforce.update_column(to_be_updated, 'Effective_End_Date__c',
                                                      (datetime.today() - timedelta(days=1)).strftime("%Y-%m-%d"))
        self.salesforce.bulk_update(object_name, to_be_updated)

        # delete future active records
        to_be_deleted = self.salesforce.query(
            "SELECT Id FROM {object_name} {filter_condition}".format(
                object_name=object_name,
                filter_condition=filter_for_sfdc + " AND  Assignment_Status__c = 'Future Active' "))
        self.salesforce.bulk_delete(object_name, to_be_deleted)

        
        # Upload - Upstream data [All columns]
        
        upstream_account = self.salesforce.get_pg_col_name('Account__c', column_details)
        query = "SELECT " + ', '.join(column_details['tb_col_nm__c']) + \
                " FROM " + self.table_name + filter_for_postgres # + " ORDER BY " + upstream_account
        df = self.postgres.download_data(query, column_details)
        df = self.salesforce.update_column(df, 'Effective_End_Date__c', team_instance.at[0, 'IC_EffEndDate__c'])
        df = self.salesforce.update_column(df, 'Effective_Start_Date__c', datetime.today().strftime("%Y-%m-%d"))
        logging.debug("updations completed. Now uploading")
        self.salesforce.bulk_upload(object_name, df)

    def future_true(self, team_instance, object_name, column_details, filter_for_sfdc, filter_for_postgres):
        #
        # Upstream df - [All columns]
        #
        upstream_account = self.salesforce.get_pg_col_name('Account__c', column_details)
        query = "SELECT " + ', '.join(column_details['tb_col_nm__c']) + \
                " FROM " + self.table_name + filter_for_postgres + " ORDER BY " + upstream_account
        # Download data from Postgres
        upstream_df = self.postgres.download_data(query)

        #
        # Implicit or explicit affiliated 
        #
        upstream_account = self.salesforce.get_pg_col_name('Account__c', column_details)
        upstream_position = self.salesforce.get_pg_col_name('Position__c', column_details)
        up_account_alignment_type = self.salesforce.get_pg_col_name('Account_Alignment_Type__c', column_details)
        up_affiliation_based = self.salesforce.get_pg_col_name('Affiliation_Based_Alignment__c', column_details)
        implicit_explicit_affiliated_query = "SELECT " + \
                                             ', '.join(column_details['tb_col_nm__c']) + \
                                             " FROM " + self.table_name + filter_for_postgres + \
                                             " AND {account_alignment_type} = 'Implicit' " + \
                                             " OR ({account_alignment_type} = 'Explicit' " + \
                                             " AND lower({affiliation_based}) = 'true') "
        implicit_explicit_affiliated_query = implicit_explicit_affiliated_query.format(
            upstream_account=upstream_account,
            account_alignment_type=up_account_alignment_type,
            affiliation_based=up_affiliation_based)
        implicit_explicit_affiliated = self.postgres.download_data(implicit_explicit_affiliated_query)

        #
        # Explicit and affiliation false [All columns insert]
        #
        explicit_query = "SELECT " + \
                         ', '.join(column_details['tb_col_nm__c']) + \
                         " FROM " + self.table_name + filter_for_postgres + \
                         " AND {account_alignment_type} = 'Explicit' " + \
                         " AND lower({affiliation_based}) = 'false' "
        explicit_query = explicit_query.format(
            account_alignment_type=up_account_alignment_type,
            affiliation_based=up_affiliation_based)
        explicit = self.postgres.download_data(explicit_query)

        #
        # Compare table
        #
        dataset_column_details = self.fetch_column_details(self.compare_dataset_id)
        pg_id = self.salesforce.get_pg_col_name('Id', dataset_column_details)
        pg_account = self.salesforce.get_pg_col_name('Account__c', dataset_column_details)
        pg_position = self.salesforce.get_pg_col_name('Position__c', dataset_column_details)
        pg_account_alignment_type = self.salesforce.get_pg_col_name('Account_Alignment_Type__c', dataset_column_details)
        pg_affiliation_based = self.salesforce.get_pg_col_name('Affiliation_Based_Alignment__c', dataset_column_details)
        compare_table_query = "SELECT {pg_id}, {pg_account} FROM " + self.compare_table_name + self.compare_table_filter
        compare_table_query = compare_table_query.format(pg_id=pg_id, pg_account=pg_account)
        compare_table = self.postgres.download_data(compare_table_query)

        to_be_deleted = compare_table[compare_table[pg_account].isin(implicit_explicit_affiliated[upstream_account])].loc[:, pg_id]
        self.salesforce.bulk_delete(object_name, to_be_deleted)
        
        #
        # Delete - records not found in upstream when compared with compare table
        #
        compare_table_query = "SELECT {pg_id}, {pg_account}, {pg_position} FROM " +\
                              self.compare_table_name + self.compare_table_filter + \
                              " AND {pg_account_alignment_type} ='Explicit' AND {pg_affiliation_based} = 'false' "
        compare_table_query = compare_table_query.format(pg_id=pg_id,
                                                         pg_account=pg_account,
                                                         pg_position=pg_position,
                                                         pg_account_alignment_type=pg_account_alignment_type,
                                                         pg_affiliation_based=pg_affiliation_based)
        compare_table = self.postgres.download_data(compare_table_query)
        to_be_deleted = compare_table[~(compare_table[pg_account] + compare_table[pg_position]).isin(upstream_df[upstream_account] + upstream_df[upstream_position])].loc[:, pg_id]
        self.salesforce.bulk_delete(object_name, to_be_deleted)

        #
        # Upstream data
        #
        implicit_explicit_affiliated.columns = column_details['Source_Column__c']
        implicit_explicit_affiliated = self.salesforce.update_column(implicit_explicit_affiliated, 'Effective_End_Date__c', team_instance.at[0, 'IC_EffEndDate__c'])
        implicit_explicit_affiliated = self.salesforce.update_column(implicit_explicit_affiliated, 'Effective_Start_Date__c', team_instance.at[0, 'IC_EffstartDate__c'])
        self.salesforce.bulk_upload(object_name, implicit_explicit_affiliated)

        explicit = explicit[~(explicit[upstream_account] + explicit[upstream_position]).isin(compare_table[pg_account]
                                                                                             + compare_table[pg_position])]
        explicit.columns = column_details['Source_Column__c']
        self.salesforce.bulk_upload(object_name, explicit)

        # # delete records
        # dataset_column_detail = self.fetch_column_details(self.compare_dataset_id)
        # pg_id = self.salesforce.get_pg_col_name('Id', dataset_column_detail)
        # pg_account = self.salesforce.get_pg_col_name('Account__c', dataset_column_detail)

        # compare_table_query = "SELECT {pg_id}, {pg_account} FROM {pg_table} {filter_condition}".format(
        #     pg_id=pg_id, pg_account=pg_account, pg_table=self.compare_table_name,
        #     filter_condition=self.compare_table_filter)
        # compare_table = self.postgres.download_data(compare_table_query)

        # upstream_to_be_deleted = compare_table[compare_table[pg_account].isin(df[self.namespace + 'Account__c'])].loc[:, pg_id]

        # cust_column_detail = self.fetch_column_details(self.customer_univ_dataset_id)
        # cust_pg_id = self.salesforce.get_pg_col_name('Id', cust_column_detail)
        # cust_to_be_deleted_query = "SELECT {id} FROM {table_name} ".format(id=cust_pg_id,
        #                                                                    table_name=self.customer_univ_table_name)
        # cust_to_be_deleted = self.postgres.download_data(cust_to_be_deleted_query)
        # cust_to_be_deleted = compare_table[compare_table[pg_account].isin(cust_to_be_deleted[cust_pg_id])].loc[:, pg_id]

        # to_be_deleted = pd.concat([upstream_to_be_deleted, cust_to_be_deleted], ignore_index=True).drop_duplicates(keep='first')
        # self.salesforce.bulk_delete(object_name, to_be_deleted)

        # self.salesforce.bulk_upload(object_name, df)
        # self.mark_success_for(object_name)

    def future_false(self, team_instance, object_name, column_details, filter_for_sfdc, filter_for_postgres):
        # delete records from SFDC
        to_be_deleted = self.salesforce.query(
            "SELECT Id FROM {object_name} {filter_condition}".format(
                object_name=object_name, filter_condition=filter_for_sfdc))
        self.salesforce.bulk_delete(object_name, to_be_deleted)

        # Download data from Postgres
        upstream_account = self.salesforce.get_pg_col_name('Account__c', column_details)
        query = "SELECT " + ', '.join(column_details['tb_col_nm__c']) + \
                " FROM " + self.table_name + filter_for_postgres + " ORDER BY " + upstream_account
        df = self.postgres.download_data(query, column_details)
        df = self.salesforce.update_column(df, 'Effective_End_Date__c', team_instance.at[0, 'IC_EffEndDate__c'])
        df = self.salesforce.update_column(df, 'Effective_Start_Date__c', team_instance.at[0, 'IC_EffstartDate__c'])
        self.salesforce.bulk_upload(object_name, df)
        self.mark_success_for(object_name)

    def past_true(self):
        pass

    def past_false(self):
        pass

    def mark_component(self):
        logging.debug("Marking components")
        query1 = "select  sfdc_id,scenario_id, status from t_scn_rule_instance_details  where scenario_id = '{}'".format(self.scenario_id)
        query2 = "select sfdc_id, scenario_rule_instance_details,status from t_business_rule where \
                  scenario_rule_instance_details in (SELECT sfdc_id FROM t_scn_rule_instance_details \
                  where scenario_id = '{}')".format(self.scenario_id)
        query3 = "select sfdc_id,scenario_id,status from t_config_call_balancing_map  where scenario_id = '{}'".format(self.scenario_id)

        df1 = self.postgres.return_output(query1)
        df2 = self.postgres.return_output(query2)
        # df3 = self.postgres.return_output(query3)
        self.flag = True

        if df1[2].str.lower().any() != 'success':
            self.flag = False
        Scenario_Rule_Instance_Details_df = DataFrame()
        Scenario_Rule_Instance_Details_df['Id'] = df1[0]
        Scenario_Rule_Instance_Details_df[self.namespace + 'Scenario_Id__c'] = df1[1]
        Scenario_Rule_Instance_Details_df[self.namespace + 'Status__c'] = df1[2]
        self.salesforce.bulk_update('Scenario_Rule_Instance_Details__c',Scenario_Rule_Instance_Details_df)
        
        if not df2.empty:
            if df2[2].str.lower().any() != 'success':
                self.flag = False
            Business_Rules_df = DataFrame()
            Business_Rules_df['Id'] = df2[0]
            Business_Rules_df[self.namespace + 'ScenarioRuleInstanceDetails__c'] = df2[1]
            Business_Rules_df[self.namespace + 'Status__c'] = df2[2]
            self.salesforce.bulk_update('Business_Rules__c',Business_Rules_df)

        #####
        # for i in range(len(df3)):
        #     self.salesforce.compound_update("Config_Call_Balancing_Map__c",df3.at[i,0],{
        #         "Scenario__c" : df3.at[i,1],
        #         "Status__c" : df3.at[i,2]
        #         })