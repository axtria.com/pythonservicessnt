import os
import json
import logging
import requests
import traceback
import simple_salesforce

from time import sleep
from datetime import datetime, timezone
from pandas import DataFrame, read_csv, merge

from src.vendor.postgres import Postgres
from src.service.register import register_job
from src.vendor.salesforce import SalesforceConnector


@register_job("DeltaSfdcToPostgres")
class DeltaSfdcToPostgres:
    def __init__(self, data_object_ids: str, scenario_id: str, user_name: str, password: str, security_token: str,
                 namespace: str, sandbox: bool, logger_id:str, logger_object_name: str, called_independently: bool):
        try:
            self.data_object_ids = list(filter(lambda x: x, data_object_ids.split(',')))
            self.scenario_id = scenario_id
            self.salesforce = SalesforceConnector.initialise_from_pool(user_name, password, security_token, namespace, sandbox)
            self.scenario_start_datetime = None
            self.data_object_start_datetime = None
            self.logger_id = logger_id
            self.logger_object_name = logger_object_name
            self.ready_for_run_flag = False
            self.called_independently = called_independently
            self.postgres = Postgres.initialise_from(self.salesforce)

        except simple_salesforce.exceptions.SalesforceAuthenticationFailed as auth_err:
            logging.error(auth_err)
            raise auth_err
            
        except Exception as e:
            if scenario_id:
                self.mark_fail(e)
                raise e 
            for do_id in self.data_object_ids:
                self.mark_fail(e, do_id)
                raise e

    def invoke(self):
        try:
            self.prepare_scenario()                             #updates scenario's last sync start date
            for data_object_id in self.data_object_ids:         #loops over data_object_ids provided in URL
                for i in range(6):   #try to sync each data object a max of 5 times in case of connection failure
                    logging.warning("Trying to sync {} for the {} time".format(data_object_id, i+1)) if i > 0 else ''
                    try:
                        if i == 5:
                            status = "Network Connectivity limited or Salesforce server unavailable. Try again later"
                            logging.critical(status)
                            self.mark_fail(status, data_object_id)
                            return status
                        self.data_object_start_datetime = self.current_datetime()
                        if not self.scenario_id:                        #no scenario id means operations performed on DataObject level.
                            self.salesforce.compound_update("Data_Object__c", data_object_id, 
                                                           {"Current_Sync_Date__c" : self.data_object_start_datetime,
                                                            "Status__c" : "In Progress"})
                        data_object = self.fetch_data_object(data_object_id)

                        # Attributes
                        logging.debug("Fetching Attributes")
                        dataset_id = data_object.loc[0, 'dataset_id__c']
                        logging.debug("dataset_id : {}".format(dataset_id))
                        object_name = data_object.loc[0, 'Data_Set_Object_Name__c']
                        suc_sync_start_date = data_object.loc[0, 'Last_Success_Sync_Start_Date__c']

                        if not suc_sync_start_date:                 #indicates first time scenario run
                            suc_sync_start_date = datetime.strptime('01-01-1900', '%d-%m-%Y').strftime('%Y-%m-%dT%H:%M:%S.%fZ')
                        table_name = data_object.loc[0, 'Final_Table__c']
                        raw_column_details = self.fetch_column_details(dataset_id)         #fetches the SFDC- PG table name mapping from DSCD
                        #logging.debug("raw_column_details : {}".format(raw_column_details))      
                        column_details = raw_column_details.dropna(axis=0)
                        self.create_postgres_table(table_name, raw_column_details)
                        filter_for_sfdc, filter_for_postgres = self.generate_filter_condition(object_name, column_details,
                                                                                              suc_sync_start_date)
                        # if not object_name.lower().endswith("position_account__c"):
                            # logging.warning("Skipping deleted records syncing for {}".format(object_name))
                        is_deleted_records = self.fetch_deleted_records(object_name, filter_for_sfdc)   #isDeleted = True to fetch deleted records
                        #self.delete_from_postgres(is_deleted_records, table_name, column_details, filter_for_postgres)
                        if not is_deleted_records.empty:
                            self.delete_records_from_pg(is_deleted_records,column_details,table_name)                
                        sfdc_file = self.fetch_master_records(object_name, column_details, filter_for_sfdc)
                        with open(sfdc_file, encoding='UTF-8') as f:
                            f.readline()            
                            a = f.readline()
                        # if not master_records.empty:
                        if a:       #indicates csv file not empty
                            logging.debug("Reading Ids if records fetched, to be deleted from PG")
                            master_records = read_csv(sfdc_file, usecols=['Id'])
                            self.delete_records_from_pg(master_records, column_details, table_name)
                            del master_records
                            self.copy_to_database(sfdc_file, table_name, column_details, filter_for_postgres)
                        if not self.scenario_id:
                            self.mark_success(self.data_object_start_datetime, data_object_id)
                        break
                    except (requests.RequestException, simple_salesforce.exceptions.SalesforceGeneralError) as e:
                        logging.exception("ERROR. Network Exception")
                        status = "Network Connectivity lost or server unavailable while syncing . Waiting for 2 minutes before trying again"
                        logging.error(status)
                        sleep(120)
                        continue
            self.mark_scenario_success()
            self.postgres.close()
            return ' '
        except Exception as e:
            logging.exception("Error")
            self.mark_fail(e, data_object_id)
            self.postgres.close()
            return str(e)

    def prepare_scenario(self):
        if self.scenario_id:
            self.scenario_start_datetime = self.current_datetime()
            # self.salesforce.update("Scenario__c", self.scenario_id, "Last_Sync_Start_Date__c",
            #                        self.scenario_start_datetime)
            self.salesforce.compound_update('Scenario__c', self.scenario_id, {
                                                                                'Last_Sync_Start_Date__c' : self.scenario_start_datetime,
                                                                                'Rule_Execution_Status__c':'In Progress',
                                                                                'Run_Response_Details__c' : ''
                                                                             })

    def fetch_data_object(self, data_object_id):
        logging.debug("fetching data object")
        return self.salesforce.query(
            "SELECT dataset_id__c, Data_Set_Object_Name__c, Final_Table__c, "
            "Last_Success_Sync_Start_Date__c "
            "FROM Data_Object__c "
            "WHERE Id = '{data_object_id}'".format(data_object_id=data_object_id))

    def fetch_column_details(self, dataset_id):
        logging.debug("Fetching column_details")
        return self.salesforce.query(
            "SELECT Source_Column__c, tb_col_nm__c, datatype__c, Table_Data_Type__c "
            "FROM Data_Set_Column_Detail__c "
            "WHERE dataset_id__c = '{dataset_id}'".format(
                dataset_id=dataset_id)
        )

    def current_datetime(self):
        return datetime.now(timezone.utc).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4] + "Z"

    def generate_filter_condition(self, object_name, column_details, suc_sync_start_date):
        logging.debug("Generating filter conditions")
        filter_for_sfdc = ''
        filter_for_postgres = ' WHERE 1=1 '
        # try:
        if self.scenario_id:
            team_instance = self.salesforce.query(
                "SELECT Team_Instance__c, Last_Sync_Success_Date__c FROM Scenario__c "
                "WHERE Id = '{scenario_id}'".format(scenario_id=self.scenario_id))
            team_instance_col_name = self.salesforce.get_pg_col_name('Team_Instance__c', column_details)
            filter_for_sfdc = " WHERE Team_Instance__c = '" + team_instance.loc[0, 'Team_Instance__c'] + "'"
            filter_for_postgres = " WHERE " + team_instance_col_name + "= '" + team_instance.loc[
                0, 'Team_Instance__c'] + "'"
            Last_Sync_Success_Date = team_instance.loc[0, "Last_Sync_Success_Date__c"]
            # if not Last_Sync_Success_Date:
            #     self.ready_for_run_flag = True
            if Last_Sync_Success_Date:
                filter_for_sfdc += " AND SystemModstamp > {Last_Sync_Success_Date}".format(
                    Last_Sync_Success_Date=Last_Sync_Success_Date.replace('+0000', 'Z'))
        else:
            filter_for_sfdc = " WHERE SystemModstamp > {}".format(suc_sync_start_date.replace('+0000', 'Z'))
        # except Exception as e:
        #     logging.exception("Error")
        return filter_for_sfdc, filter_for_postgres

    def fetch_master_records(self, object_name, column_details, filter_str):
        logging.debug("Fetching Master records")
        column_details_str = ', '.join(column_details['Source_Column__c'])
        bulk_query = "SELECT " + column_details_str + " FROM " + object_name + filter_str
        return self.salesforce.bulk_download(object_name, bulk_query)

    def create_postgres_table(self, table_name: str, column_details: DataFrame):
        logging.debug("Creating postgres table")
        schema = '( '
        for index, row in column_details.iterrows():
            if (row["Table_Data_Type__c"].lower() != "text"):
                schema += row["tb_col_nm__c"] + " " + row["Table_Data_Type__c"].lower() + ", "
            else:
                schema += row["tb_col_nm__c"] + " " + "varchar, "
        schema += 'pg_created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP )'
        schema = schema[:-2] + ' )'
        create_table_query = "create table IF NOT EXISTS {} {}".format(table_name, schema)
        self.postgres.execute_query(create_table_query)

    def fetch_deleted_records(self, object_name, filter_str):
        logging.debug("Fetching is_deleted_records")
        if not filter_str.startswith(" WHERE SystemModstamp > 1900"):
            query_str = "SELECT Id FROM " + object_name + filter_str + " AND isDeleted = TRUE "
        else:
            query_str = "SELECT Id FROM " + object_name  + " WHERE isDeleted = TRUE "
        if query_str.find(" AND ") != -1 and query_str.find(" WHERE ") == -1:
            logging.criticial("Malformed query for detching isDeleted records.")
            raise Exception("Malformed query for detching isDeleted records.")
        return self.salesforce.query_all(query_str)
        # for i in range(5):
        #     try:
        #         return self.salesforce.query_all(query_str)
        #     except Exception as e:
        #         logging.error(e)
        #         continue

    def delete_records_from_pg(self, records, column_details, table_name):
        temp_table_name = 'temp_to_be_del_'+datetime.now(timezone.utc).strftime('%Y_%m_%d__%H_%M_%S_%f')
        file_path = os.path.abspath(os.path.join('tmp', temp_table_name) + '.csv')
        records.to_csv(file_path, index=None)
        logging.debug('Records to be deleted: '+str(len(records)))

        create_table_query = "CREATE TABLE IF NOT EXISTS {} (Id text)".format(temp_table_name)
        logging.debug('Creating temp table on postgres')
        self.postgres.execute_query(create_table_query)

        logging.debug('Copying records to Postgres')
        self.postgres.execute_query("COPY {} FROM '{}' DELIMITER ',' CSV HEADER".format(temp_table_name, file_path))

        # pg_id_col = str(column_details.loc[column_details['Source_Column__c'].lower()=='id','tb_col_nm__c'])
        # logging.debug('Fetching PG_col_name of Id')
        pg_id_col = self.salesforce.get_pg_col_name('Id', column_details)
        optimized_delete_query = "select public.fn_delete_from_table('{}','{}','{}','Id')".format(table_name,temp_table_name,pg_id_col)
        logging.debug('Running optimized_delete_query')
        self.postgres.execute_query(optimized_delete_query)
        logging.debug('Dropping temp table from Postgres')
        drop_table_query = 'DROP TABLE '+ temp_table_name
        self.postgres.execute_query(drop_table_query)

    # def delete_from_postgres(self, df, table_name, column_details, filter_str):
    #     try:
    #         if self._is_valid(df):
    #             pg_col_id = self.salesforce.get_pg_col_name('Id', column_details)
    #             sfdc_ids = "'" + "', '".join(df['Id']) + "'"
    #             filter_str += " AND {} in ({})".format(pg_col_id, sfdc_ids)
    #             self.postgres.execute_query("DELETE FROM " + table_name + filter_str)
    #     except Exception as e:
    #         logging.warning(e)

    def copy_to_database(self, filename, table_name, column_details, filter_str):
        logging.debug("Copying to datbase")
        # if self._is_valid(df):

        # pg_id = self.salesforce.get_pg_col_name('Id', column_details)
        # ids = "'" + "', '".join(df['Id']) + "'"
        # self.postgres.execute_query(
        #     "DELETE FROM " + table_name + " WHERE {pg_id} IN ({ids})".format(pg_id=pg_id, ids=ids))
        df = read_csv(filename, nrows=1)
        ordered_pg_columns = self._fetch_ordered_pg_columns(column_details, df)
        self.postgres.execute_query("COPY " + table_name +
                                    "(" + ordered_pg_columns +
                                    ") FROM '" + filename + "' DELIMITER ',' CSV HEADER")

    def mark_success(self, current_dtime, data_object_id):
        self.salesforce.compound_update('Data_Object__c', data_object_id, {
            'Status__c': 'Success',
            'Last_Success_Sync_Start_Date__c': current_dtime,
            'Last_Success_Sync_End_Date__c': self.current_datetime()
        })


    def mark_scenario_success(self):
        if self.scenario_id:
            if self.called_independently:
                self.salesforce.compound_update('Scenario__c', self.scenario_id, {'Last_Sync_Success_Date__c':self.scenario_start_datetime,'Rule_Execution_Status__c':'Ready for Run'})
            else:
                self.salesforce.update('Scenario__c', self.scenario_id, 'Last_Sync_Success_Date__c',self.scenario_start_datetime)

        if self.logger_object_name:
            logging.debug("\n\n Marking logger object as success")
            self.salesforce.update(self.logger_object_name,self.logger_id,"Status__c","Success")

    def mark_fail(self, e, data_object_id=None):
        if self.scenario_id:
            logging.error("Marking error for scenario rule_execution_status")
            self.salesforce.compound_update('Scenario__c', self.scenario_id, {
                'Rule_Execution_Status__c': 'Error',
                'Run_Response_Details__c': str(e)
            })
        else:
            logging.error("Marking error in Data_Object__c and Logger object {}".format(self.logger_object_name))
            try:
                self.salesforce.compound_update('Data_Object__c', data_object_id,{
                    'Status__c': 'Error',
                    'Sync_Response_Detail__c': str(e)[:1999]})
                self.salesforce.compound_update(self.logger_object_name, self.logger_id, {
                    "Status__c": "Error",
                    "Stack_Trace__c": str(e)})
            except Exception as e:
                logging.exception("Error")

    def _is_valid(self, df: DataFrame):
        if len(df) > 0:
            logging.debug(" No . of records %d" % len(df))
            return True
        else:
            logging.warning(" No. of records found to be 0.")
            return False

    def _fetch_ordered_pg_columns(self, column_details, df) -> str:
        try:
            column_details['Source_Column__c'] = column_details['Source_Column__c'].str.lower()
            actual_columns = DataFrame(list(df.columns.str.lower()), columns=['actual_order'])
            ordered_pg_columns = merge(actual_columns, column_details, left_on='actual_order',
                                       right_on='Source_Column__c', how='left')['tb_col_nm__c']
            logging.debug("ordered_pg_columns : {}".format(ordered_pg_columns))
            return ', '.join(ordered_pg_columns)
        except TypeError as e:
            logging.exception(e)
            raise TypeError("Check your Source_Column__c and tb_col_nm__c mapping in Data_Set_Column_Detail__c")