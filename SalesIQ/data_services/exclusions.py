unmanaged_objects = ["SIQ_Prospects__c, Custom_Object__c, Product_Catalog__c"]   #custom ojects not part of managed package

unmanaged_fields = {} 
#custom object part of managed package but some of its custom fields not a part 
unmanaged_fields["Position_Geography__c"] = ["Quarter__c", "ZIP__c"]
unmanaged_fields["Account"] = ["AxtriaARSnT__AZ_Internal_Speciality__c","AxtriaARSnT__Customer_SubType__c","AxtriaARSnT__Segment1__c","AxtriaARSnT__Segment2__c","AxtriaARSnT__Segment3__c","AxtriaARSnT__Segment4__c","AxtriaARSnT__Segment5__c","AxtriaARSnT__Segment6__c","AxtriaARSnT__Segment7__c","AxtriaARSnT__Role__c","AxtriaARSnT__Billing_Country__c" ]
unmanaged_fields["Account_Address__c"] = ["AxtriaARSnT__Status__c"]
unmanaged_fields["Position_Product__c"] = ["AxtriaARSnT__Product_Catalog__c", "AxtriaARSnT__Product_Code__c", "AxtriaARSnT__Product_Catalog__r", "Product_Code__c", "Product_Catalog__c", "Product_Catalog__r"]
unmanaged_fields["Account_Affiliation__c"] = ["AxtriaARSnT__Affiliation_Status__c"]
unmanaged_fields["Account_Exclusion__c"] = ["Position__c", "Team_Instance__c", "Position__r.Name", "Position__r.AxtriaSalesIQTM__Client_Position_Code__c"]